import 'package:built_collection/built_collection.dart';
import 'package:built_value/built_value.dart';
import 'package:built_value/serializer.dart';

part 'data_models.g.dart';


abstract class ShelvesPageRequest implements Built<ShelvesPageRequest, ShelvesPageRequestBuilder> {
  Summary get summary;
  Shelf get shelf;
  Entities get entities;
  ShelvesPageRequest._();
  factory ShelvesPageRequest([updates(ShelvesPageRequestBuilder b)]) = _$ShelvesPageRequest;
  static Serializer<ShelvesPageRequest> get serializer => _$shelvesPageRequestSerializer;

}
abstract class Summary implements Built<Summary, SummaryBuilder> {
  BuiltList<ShelfSummary> get shelves;

  Summary._();
  factory Summary([updates(SummaryBuilder b)]) = _$Summary;
  static Serializer<Summary> get serializer => _$summarySerializer;
}

abstract class ShelfSummary implements Built<ShelfSummary, ShelfSummaryBuilder> {
  String get name;
  num get count;
  ShelfSummary._();
  factory ShelfSummary([updates(ShelfSummaryBuilder b)]) = _$ShelfSummary;
  static Serializer<ShelfSummary> get serializer => _$shelfSummarySerializer;
}


abstract class Shelf implements Built<Shelf, ShelfBuilder> {
  int get userId;
  String get name;
  bool get privateByDefault;
  BuiltList<int> get items;

  Shelf._();
  factory Shelf([updates(ShelfBuilder b)]) = _$Shelf;

  static Serializer<Shelf> get serializer => _$shelfSerializer;
}

abstract class Entities implements Built<Entities, EntitiesBuilder> {
  BuiltMap<String, Bib> get bibs;
  Entities._();
  factory Entities([updates(EntitiesBuilder b)]) = _$Entities;

  static Serializer<Entities> get serializer => _$entitiesSerializer;
}

abstract class Bib implements Built<Bib,BibBuilder> {
  String get id;
  num get sourceLibId;


  Bib._();
  factory Bib([updates(BibBuilder b)]) = _$Bib;
  static Serializer<Bib> get serializer => _$bibSerializer;
}

abstract class BriefInfo implements Built<BriefInfo, BriefInfoBuilder>{

  String get title;
  String get subtitle;
  String get format;
  BuiltList<String> get superFormats;
  String get consumptionFormat;
  String get callNumber;
  BuiltList<String> get subjectHeadings;


  BriefInfo._();
  factory BriefInfo([updates(BriefInfoBuilder b)]) = _$BriefInfo;

  static Serializer<BriefInfo> get serializer => _$briefInfoSerializer;

}

abstract class Jacket implements Built<Jacket, JacketBuilder> {
  String get type;
  String get small;
  String get medium;
  String get large;

  static Serializer<Jacket> get serializer => _$jacketSerializer;

  Jacket._();

  factory Jacket([updates(JacketBuilder b)]) = _$Jacket;
}

abstract class Availability implements Built<Availability, AvailabilityBuilder> {
  Availability._();
  factory Availability([updates(AvailabilityBuilder b)]) = _$Availability;

  static Serializer<Availability> get serializer => _$availabilitySerializer;
}


