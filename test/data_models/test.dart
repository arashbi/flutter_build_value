import 'package:flutter_app/models/data_models.dart';
import 'package:test/test.dart';
import 'package:flutter_app/models/serializers.dart';
import 'dart:convert';
main() {
  const fullRequest =
  """
     {
  "summary": {
    "shelves": [
      {
        "name": "for_later",
        "count": 42
      },
      {
        "name": "completed",
        "count": 62
      },
      {
        "name": "in_progress",
        "count": 28
      }
    ]
  },
  "shelf": {
    "userId": 719393399,
    "name": "in_progress",
    "privateByDefault": false,
    "query": {
      "filters": {}
    },
    "items": [
      1260564627,
      1246957497,
      1225636297,
      1192687271,
      1192687269,
      1192570667,
      1167048398,
      1167048397,
      1167048537,
      1167048438,
      1167048517,
      1167048417,
      1167048437,
      1162520457,
      1162496497,
      1126049677,
      1124746344,
      1114177685,
      1114177287,
      1114175944,
      1077982549,
      1048567537,
      935072479,
      933187694,
      933906277
    ],
    "facets": [
      {
        "hasMore": false,
        "id": "FORMAT",
        "fieldFilters": [
          {
            "groupIds": [
              "BOOKS"
            ],
            "label": "",
            "value": "BK",
            "applied": false,
            "count": 13
          },
          {
            "groupIds": [
              "BOOKS"
            ],
            "label": "",
            "value": "EBOOK",
            "applied": false,
            "count": 6
          },
          {
            "groupIds": [
              "BOOKS"
            ],
            "label": "",
            "value": "GRAPHIC_NOVEL",
            "applied": false,
            "count": 2
          },
          {
            "groupIds": [
              "MUSIC_SOUND"
            ],
            "label": "",
            "value": "MUSIC_CD",
            "applied": false,
            "count": 2
          },
          {
            "groupIds": [
              "AUDIOBOOKS_SPOKEN_WORD"
            ],
            "label": "",
            "value": "AB",
            "applied": false,
            "count": 1
          },
          {
            "groupIds": [
              "AUDIOBOOKS_SPOKEN_WORD"
            ],
            "label": "",
            "value": "BOOK_CD",
            "applied": false,
            "count": 1
          },
          {
            "groupIds": [
              "MOVIES_TV_VIDEO"
            ],
            "label": "",
            "value": "DVD",
            "applied": false,
            "count": 1
          },
          {
            "groupIds": [
              "BOOKS",
              "ACCESSIBLE_FORMATS"
            ],
            "label": "",
            "value": "LPRINT",
            "applied": false,
            "count": 1
          },
          {
            "groupIds": [
              "OTHER"
            ],
            "label": "",
            "value": "UK",
            "applied": false,
            "count": 1
          }
        ]
      },
      {
        "hasMore": false,
        "id": "SUPERFORMAT",
        "fieldFilters": [
          {
            "groupIds": [],
            "label": "",
            "value": "BOOKS",
            "applied": false,
            "count": 22
          },
          {
            "groupIds": [],
            "label": "",
            "value": "AUDIOBOOKS_SPOKEN_WORD",
            "applied": false,
            "count": 2
          },
          {
            "groupIds": [],
            "label": "",
            "value": "MUSIC_SOUND",
            "applied": false,
            "count": 2
          },
          {
            "groupIds": [],
            "label": "",
            "value": "ACCESSIBLE_FORMATS",
            "applied": false,
            "count": 1
          },
          {
            "groupIds": [],
            "label": "",
            "value": "MOVIES_TV_VIDEO",
            "applied": false,
            "count": 1
          },
          {
            "groupIds": [],
            "label": "",
            "value": "OTHER",
            "applied": false,
            "count": 1
          }
        ]
      },
      {
        "hasMore": true,
        "id": "TOPIC_HEADINGS",
        "fieldFilters": [
          {
            "groupIds": [],
            "label": "",
            "value": "Police",
            "applied": false,
            "count": 5
          },
          {
            "groupIds": [],
            "label": "",
            "value": "Human-animal relationships",
            "applied": false,
            "count": 3
          },
          {
            "groupIds": [],
            "label": "",
            "value": "Men with disabilities",
            "applied": false,
            "count": 3
          },
          {
            "groupIds": [],
            "label": "",
            "value": "Montalván, Luis Carlos",
            "applied": false,
            "count": 3
          },
          {
            "groupIds": [],
            "label": "",
            "value": "Police misconduct",
            "applied": false,
            "count": 3
          },
          {
            "groupIds": [],
            "label": "",
            "value": "Service dogs",
            "applied": false,
            "count": 3
          },
          {
            "groupIds": [],
            "label": "",
            "value": "Carnival",
            "applied": false,
            "count": 2
          },
          {
            "groupIds": [],
            "label": "",
            "value": "Crowds",
            "applied": false,
            "count": 2
          },
          {
            "groupIds": [],
            "label": "",
            "value": "Popular music",
            "applied": false,
            "count": 2
          },
          {
            "groupIds": [],
            "label": "",
            "value": "Riots",
            "applied": false,
            "count": 2
          },
          {
            "groupIds": [],
            "label": "",
            "value": "Rock music",
            "applied": false,
            "count": 2
          },
          {
            "groupIds": [],
            "label": "",
            "value": "Seattle (Wash.)",
            "applied": false,
            "count": 2
          },
          {
            "groupIds": [],
            "label": "",
            "value": "Shrove Tuesday",
            "applied": false,
            "count": 2
          },
          {
            "groupIds": [],
            "label": "",
            "value": "Aliens",
            "applied": false,
            "count": 1
          },
          {
            "groupIds": [],
            "label": "",
            "value": "Alphabet",
            "applied": false,
            "count": 1
          }
        ]
      },
      {
        "hasMore": true,
        "id": "AUTHOR",
        "fieldFilters": [
          {
            "groupIds": [],
            "label": "",
            "value": "Montalván, Luis Carlos",
            "applied": false,
            "count": 3
          },
          {
            "groupIds": [],
            "label": "",
            "value": "Aciman, Alexander",
            "applied": false,
            "count": 1
          },
          {
            "groupIds": [],
            "label": "",
            "value": "Ashton-Wolfe, H.",
            "applied": false,
            "count": 1
          },
          {
            "groupIds": [],
            "label": "",
            "value": "Bloch, Robert",
            "applied": false,
            "count": 1
          },
          {
            "groupIds": [],
            "label": "",
            "value": "Chartier, Jaq",
            "applied": false,
            "count": 1
          },
          {
            "groupIds": [],
            "label": "",
            "value": "Davick, Linda",
            "applied": false,
            "count": 1
          },
          {
            "groupIds": [],
            "label": "",
            "value": "Domanick, Joe",
            "applied": false,
            "count": 1
          },
          {
            "groupIds": [],
            "label": "",
            "value": "Drake, Jane",
            "applied": false,
            "count": 1
          },
          {
            "groupIds": [],
            "label": "",
            "value": "Gabriel, Peter",
            "applied": false,
            "count": 1
          },
          {
            "groupIds": [],
            "label": "",
            "value": "Gervay, Susanne",
            "applied": false,
            "count": 1
          },
          {
            "groupIds": [],
            "label": "",
            "value": "Grant, Pat",
            "applied": false,
            "count": 1
          },
          {
            "groupIds": [],
            "label": "",
            "value": "Jones, Quincy",
            "applied": false,
            "count": 1
          },
          {
            "groupIds": [],
            "label": "",
            "value": "Keller, A. G.",
            "applied": false,
            "count": 1
          },
          {
            "groupIds": [],
            "label": "",
            "value": "King, Stephen",
            "applied": false,
            "count": 1
          },
          {
            "groupIds": [],
            "label": "",
            "value": "Kingsbury, Karen",
            "applied": false,
            "count": 1
          }
        ]
      },
      {
        "hasMore": true,
        "id": "GENRE_HEADINGS",
        "fieldFilters": [
          {
            "groupIds": [],
            "label": "",
            "value": "Electronic Books",
            "applied": false,
            "count": 6
          },
          {
            "groupIds": [],
            "label": "",
            "value": "Fiction",
            "applied": false,
            "count": 6
          },
          {
            "groupIds": [],
            "label": "",
            "value": "Biography",
            "applied": false,
            "count": 4
          },
          {
            "groupIds": [],
            "label": "",
            "value": "Juvenile Fiction",
            "applied": false,
            "count": 3
          },
          {
            "groupIds": [],
            "label": "",
            "value": "Juvenile Literature",
            "applied": false,
            "count": 3
          },
          {
            "groupIds": [],
            "label": "",
            "value": "Comic Books, Strips, Etc",
            "applied": false,
            "count": 2
          },
          {
            "groupIds": [],
            "label": "",
            "value": "Graphic Novels",
            "applied": false,
            "count": 2
          },
          {
            "groupIds": [],
            "label": "",
            "value": "Horror Fiction",
            "applied": false,
            "count": 2
          },
          {
            "groupIds": [],
            "label": "",
            "value": "Audiobooks",
            "applied": false,
            "count": 1
          },
          {
            "groupIds": [],
            "label": "",
            "value": "Autobiographical Comics",
            "applied": false,
            "count": 1
          },
          {
            "groupIds": [],
            "label": "",
            "value": "Biographical Films",
            "applied": false,
            "count": 1
          },
          {
            "groupIds": [],
            "label": "",
            "value": "Children's Audiobooks",
            "applied": false,
            "count": 1
          },
          {
            "groupIds": [],
            "label": "",
            "value": "Christian Fiction",
            "applied": false,
            "count": 1
          },
          {
            "groupIds": [],
            "label": "",
            "value": "Detective and Mystery Fiction",
            "applied": false,
            "count": 1
          },
          {
            "groupIds": [],
            "label": "",
            "value": "Downloadable Audio Books",
            "applied": false,
            "count": 1
          }
        ]
      },
      {
        "hasMore": false,
        "id": "AUDIENCE",
        "fieldFilters": [
          {
            "groupIds": [],
            "label": "",
            "value": "adult",
            "applied": false,
            "count": 21
          },
          {
            "groupIds": [],
            "label": "",
            "value": "juvenile",
            "applied": false,
            "count": 6
          },
          {
            "groupIds": [],
            "label": "",
            "value": "teen",
            "applied": false,
            "count": 1
          },
          {
            "groupIds": [],
            "label": "",
            "value": "undetermined",
            "applied": false,
            "count": 1
          }
        ]
      },
      {
        "hasMore": false,
        "id": "FICTION_TYPE",
        "fieldFilters": [
          {
            "groupIds": [],
            "label": "",
            "value": "NONFICTION",
            "applied": false,
            "count": 12
          },
          {
            "groupIds": [],
            "label": "",
            "value": "FICTION",
            "applied": false,
            "count": 11
          },
          {
            "groupIds": [],
            "label": "",
            "value": "UNDETERMINED",
            "applied": false,
            "count": 5
          }
        ]
      }
    ],
    "pagination": {
      "count": 28,
      "page": 1,
      "limit": 25,
      "pages": 2
    },
    "sort": {
      "direction": "des",
      "fields": [
        {
          "id": "title",
          "direction": "asc",
          "applied": false
        },
        {
          "id": "rating",
          "direction": "des",
          "applied": false
        },
        {
          "id": "author",
          "direction": "asc",
          "applied": false
        },
        {
          "id": "date_added",
          "direction": "des",
          "applied": true
        }
      ]
    }
  },
  "entities": {
    "bibs": {
      "S30C3305179": {
        "id": "S30C3305179",
        "sourceLibId": 30,
        "briefInfo": {
          "metadataId": "S30C3305179",
          "title": "Sleeping Beauties",
          "subtitle": "A Novel",
          "format": "EBOOK",
          "superFormats": [
            "BOOKS",
            "ELECTRONIC_FORMATS"
          ],
          "consumptionFormat": "READ",
          "callNumber": "EBOOK OVERDRIVE",
          "description": "In this spectacular father/son collaboration, Stephen King and Owen King tell the highest of high-stakes stories: what might happen if women disappeared from the world of men? In a future so real and near it might be now, something happens when women go to sleep; they become shrouded in a cocoon-like gauze. If they are awakened, if the gauze wrapping their bodies is disturbed or violated, the women become feral and spectacularly violent; and while they sleep they go to another place... The men of our world are abandoned, left to their increasingly primal devices. One woman, however, the mysterious Evie, is immune to the blessing or curse of the sleeping disease. Is Evie a medical anomaly to be studied? Or is she a demon who must be slain? Set in a small Appalachian town whose primary employer is a women's prison, Sleeping Beauties is a wildly provocative, gloriously absorbing father/son collaboration between Stephen King and Owen King.",
          "genreForm": [
            "Dystopian Fiction",
            "Horror fiction",
            "Fantasy fiction",
            "Fantasy Fiction",
            "Electronic books",
            "Dystopian fiction",
            "Horror Fiction",
            "Thrillers (Fiction)",
            "Fiction",
            "Electronic Books"
          ],
          "subjectHeadings": [
            "Demonology Fiction",
            "Women Fiction",
            "Horror stories Fiction",
            "Fiction",
            "Horror",
            "Suspense",
            "Thriller"
          ],
          "authors": [
            "King, Stephen"
          ],
          "publicationDate": "2017",
          "jacket": {
            "type": "SYNDETICS",
            "small": "https://secure.syndetics.com/index.aspx?isbn=9781501163425/SC.GIF&client=sepup&type=xw12&oclc=",
            "medium": "https://secure.syndetics.com/index.aspx?isbn=9781501163425/MC.GIF&client=sepup&type=xw12&oclc=",
            "large": "https://secure.syndetics.com/index.aspx?isbn=9781501163425/LC.JPG&client=sepup&type=xw12&oclc=",
            "local_url": null
          },
          "isbns": [
            "9781501163425",
            "1501163426"
          ],
          "primaryLanguage": "eng",
          "series": [],
          "edition": "First Scribner hardcover edition",
          "multiscriptTitle": null,
          "multiscriptAuthor": null,
          "id": "S30C3305179",
          "rating": {
            "averageRating": 70,
            "totalCount": 204
          }
        },
        "policy": {
          "materialType": "DIGITAL",
          "provider": "OverDriveAPI",
          "holdable": true,
          "itemLevelHoldableItems": false,
          "materialPolicy": {
            "supportsBatchVolumeHolds": false,
            "itemLevelHoldsPredetermined": false,
            "requiresDuration": false,
            "requiresEmailDuringHold": true,
            "requiresFormatDuringHold": false,
            "requiresFormatDuringCheckout": false,
            "supportsDownload": true,
            "supportsFastAvailability": false,
            "supportsReadNow": true,
            "showFormatDuringHold": true,
            "showFormatDuringCheckout": true,
            "supportsSuspendHold": true,
            "supportsSuspendHoldStartDateEdit": false,
            "supportsSuspendHoldEndDateEdit": true,
            "supportAutoCheckout": true,
            "showQuickstartLink": true,
            "supportsBorrowingHistory": false,
            "borrowingHistorySupportsReturnedDates": false,
            "maintainsBorrowingHistoryOnDisable": false,
            "supportsCancelledExpiredHolds": false,
            "helpUrl": "http://www.overdrive.com/resources/drc/Default.aspx?type=ebook",
            "quickStartUrl": "https://help.overdrive.com/#",
            "healthy": true,
            "externalAPI": true
          }
        },
        "availability": {
          "metadataId": "S30C3305179",
          "bibType": "DIGITAL",
          "availabilityLocationType": "OTHER",
          "status": "UNAVAILABLE",
          "circulationType": "REQUEST",
          "libraryUseOnly": false,
          "heldCopies": 13,
          "availableCopies": 0,
          "totalCopies": 3,
          "localisedStatus": "UNAVAILABLE",
          "singleBranch": false,
          "eresourceDescription": null,
          "eresourceUrl": null
        }
      },
      "S30C2221268": {
        "id": "S30C2221268",
        "sourceLibId": 30,
        "briefInfo": {
          "metadataId": "S30C2221268",
          "title": "Hit",
          "subtitle": "[the Definitive Two CD Collection]",
          "format": "MUSIC_CD",
          "superFormats": [
            "MUSIC_SOUND",
            "MODERN_FORMATS"
          ],
          "consumptionFormat": "LISTEN",
          "callNumber": "CD 782.42166 G1145",
          "description": null,
          "genreForm": [],
          "subjectHeadings": [
            "Rock music",
            "Popular music"
          ],
          "authors": [
            "Gabriel, Peter"
          ],
          "publicationDate": "2003",
          "jacket": {
            "type": "SYNDETICS",
            "small": null,
            "medium": null,
            "large": null,
            "local_url": null
          },
          "isbns": [],
          "primaryLanguage": "eng",
          "series": [],
          "edition": null,
          "multiscriptTitle": null,
          "multiscriptAuthor": null,
          "id": "S30C2221268",
          "rating": {
            "averageRating": 75,
            "totalCount": 8
          }
        },
        "policy": {
          "materialType": "PHYSICAL",
          "provider": "ILS",
          "holdable": true,
          "itemLevelHoldableItems": false,
          "materialPolicy": {
            "supportsBatchVolumeHolds": true,
            "itemLevelHoldsPredetermined": true,
            "requiresDuration": false,
            "requiresEmailDuringHold": false,
            "requiresFormatDuringHold": false,
            "requiresFormatDuringCheckout": false,
            "supportsDownload": false,
            "supportsFastAvailability": false,
            "supportsReadNow": false,
            "showFormatDuringHold": false,
            "showFormatDuringCheckout": false,
            "supportsSuspendHold": false,
            "supportsSuspendHoldStartDateEdit": false,
            "supportsSuspendHoldEndDateEdit": true,
            "supportAutoCheckout": false,
            "showQuickstartLink": false,
            "supportsBorrowingHistory": false,
            "borrowingHistorySupportsReturnedDates": true,
            "maintainsBorrowingHistoryOnDisable": false,
            "supportsCancelledExpiredHolds": true,
            "helpUrl": null,
            "quickStartUrl": null,
            "healthy": true,
            "externalAPI": false
          }
        },
        "availability": {
          "metadataId": "S30C2221268",
          "bibType": "PHYSICAL",
          "availabilityLocationType": "NA",
          "status": "UNAVAILABLE",
          "circulationType": "REQUEST",
          "libraryUseOnly": false,
          "heldCopies": 0,
          "availableCopies": 0,
          "totalCopies": 1,
          "localisedStatus": "UNAVAILABLE",
          "singleBranch": false,
          "eresourceDescription": null,
          "eresourceUrl": null
        }
      },
      "S30C3387619": {
        "id": "S30C3387619",
        "sourceLibId": 30,
        "briefInfo": {
          "metadataId": "S30C3387619",
          "title": "Mia",
          "subtitle": null,
          "format": "EBOOK",
          "superFormats": [
            "BOOKS",
            "ELECTRONIC_FORMATS"
          ],
          "consumptionFormat": "READ",
          "callNumber": "FRENCH EBOOK OVERDRIVE",
          "description": null,
          "genreForm": [
            "Electronic books",
            "Fiction",
            "Electronic Books"
          ],
          "subjectHeadings": [
            "Man-woman relationships Fiction",
            "Physicians Fiction",
            "Family secrets Fiction"
          ],
          "authors": [
            "Keller, A. G."
          ],
          "publicationDate": "2018",
          "jacket": {
            "type": "SYNDETICS",
            "small": "https://secure.syndetics.com/index.aspx?isbn=9781547517947/SC.GIF&client=sepup&type=xw12&oclc=",
            "medium": "https://secure.syndetics.com/index.aspx?isbn=9781547517947/MC.GIF&client=sepup&type=xw12&oclc=",
            "large": "https://secure.syndetics.com/index.aspx?isbn=9781547517947/LC.JPG&client=sepup&type=xw12&oclc=",
            "local_url": null
          },
          "isbns": [
            "9781547517947",
            "1547517948"
          ],
          "primaryLanguage": "fre",
          "series": [],
          "edition": null,
          "multiscriptTitle": null,
          "multiscriptAuthor": null,
          "id": "S30C3387619",
          "rating": {}
        },
        "policy": {
          "materialType": "DIGITAL",
          "provider": "OverDriveAPI",
          "holdable": true,
          "itemLevelHoldableItems": false,
          "materialPolicy": {
            "supportsBatchVolumeHolds": false,
            "itemLevelHoldsPredetermined": false,
            "requiresDuration": false,
            "requiresEmailDuringHold": true,
            "requiresFormatDuringHold": false,
            "requiresFormatDuringCheckout": false,
            "supportsDownload": true,
            "supportsFastAvailability": false,
            "supportsReadNow": true,
            "showFormatDuringHold": true,
            "showFormatDuringCheckout": true,
            "supportsSuspendHold": true,
            "supportsSuspendHoldStartDateEdit": false,
            "supportsSuspendHoldEndDateEdit": true,
            "supportAutoCheckout": true,
            "showQuickstartLink": true,
            "supportsBorrowingHistory": false,
            "borrowingHistorySupportsReturnedDates": false,
            "maintainsBorrowingHistoryOnDisable": false,
            "supportsCancelledExpiredHolds": false,
            "helpUrl": "http://www.overdrive.com/resources/drc/Default.aspx?type=ebook",
            "quickStartUrl": "https://help.overdrive.com/#",
            "healthy": true,
            "externalAPI": true
          }
        },
        "availability": {
          "metadataId": "S30C3387619",
          "bibType": "DIGITAL",
          "availabilityLocationType": "OTHER",
          "status": "AVAILABLE",
          "circulationType": "REQUEST",
          "libraryUseOnly": false,
          "heldCopies": 0,
          "availableCopies": 1,
          "totalCopies": 1,
          "localisedStatus": "AVAILABLE",
          "singleBranch": false,
          "eresourceDescription": null,
          "eresourceUrl": null
        }
      },
      "S30C3312725": {
        "id": "S30C3312725",
        "sourceLibId": 30,
        "briefInfo": {
          "metadataId": "S30C3312725",
          "title": "Mouse",
          "subtitle": "",
          "format": "BK",
          "superFormats": [
            "BOOKS",
            "MODERN_FORMATS"
          ],
          "consumptionFormat": "READ",
          "callNumber": "E LUDVICE",
          "description": "A hungry little mouse befriends the letter M, discovering the alphabet nibble by nibble along the way--",
          "genreForm": [
            "Picture Books",
            "Picture books",
            "Juvenile Fiction"
          ],
          "subjectHeadings": [
            "Mice Juvenile fiction",
            "Friendship Juvenile fiction",
            "Alphabet"
          ],
          "authors": [
            "Ludvicek, Zebo"
          ],
          "publicationDate": "2017",
          "jacket": {
            "type": "SYNDETICS",
            "small": "https://secure.syndetics.com/index.aspx?isbn=9781101996362/SC.GIF&client=sepup&type=xw12&oclc=",
            "medium": "https://secure.syndetics.com/index.aspx?isbn=9781101996362/MC.GIF&client=sepup&type=xw12&oclc=",
            "large": "https://secure.syndetics.com/index.aspx?isbn=9781101996362/LC.JPG&client=sepup&type=xw12&oclc=",
            "local_url": null
          },
          "isbns": [
            "9781101996362",
            "1101996366"
          ],
          "primaryLanguage": "eng",
          "series": [],
          "edition": "First edition",
          "multiscriptTitle": null,
          "multiscriptAuthor": null,
          "id": "S30C3312725",
          "rating": {
            "averageRating": 75,
            "totalCount": 2
          }
        },
        "policy": {
          "materialType": "PHYSICAL",
          "provider": "ILS",
          "holdable": true,
          "itemLevelHoldableItems": false,
          "materialPolicy": {
            "supportsBatchVolumeHolds": true,
            "itemLevelHoldsPredetermined": true,
            "requiresDuration": false,
            "requiresEmailDuringHold": false,
            "requiresFormatDuringHold": false,
            "requiresFormatDuringCheckout": false,
            "supportsDownload": false,
            "supportsFastAvailability": false,
            "supportsReadNow": false,
            "showFormatDuringHold": false,
            "showFormatDuringCheckout": false,
            "supportsSuspendHold": false,
            "supportsSuspendHoldStartDateEdit": false,
            "supportsSuspendHoldEndDateEdit": true,
            "supportAutoCheckout": false,
            "showQuickstartLink": false,
            "supportsBorrowingHistory": false,
            "borrowingHistorySupportsReturnedDates": true,
            "maintainsBorrowingHistoryOnDisable": false,
            "supportsCancelledExpiredHolds": true,
            "helpUrl": null,
            "quickStartUrl": null,
            "healthy": true,
            "externalAPI": false
          }
        },
        "availability": {
          "metadataId": "S30C3312725",
          "bibType": "PHYSICAL",
          "availabilityLocationType": "OTHER",
          "status": "AVAILABLE",
          "circulationType": "REQUEST",
          "libraryUseOnly": false,
          "heldCopies": 0,
          "availableCopies": 8,
          "totalCopies": 11,
          "localisedStatus": "AVAILABLE_IN_BRANCH",
          "singleBranch": false,
          "eresourceDescription": null,
          "eresourceUrl": null
        }
      },
      "S30C3114974": {
        "id": "S30C3114974",
        "sourceLibId": 30,
        "briefInfo": {
          "metadataId": "S30C3114974",
          "title": "Lizards",
          "subtitle": "",
          "format": "EBOOK",
          "superFormats": [
            "BOOKS",
            "ELECTRONIC_FORMATS"
          ],
          "consumptionFormat": "READ",
          "callNumber": "EBOOK J OVERDRIVE",
          "description": "Introduces readers to lizards, looking at what they have in common, as well as some of the special traits and abilities that individual species have.",
          "genreForm": [
            "Juvenile Literature",
            "Electronic books",
            "Electronic Books"
          ],
          "subjectHeadings": [
            "Lizards Juvenile literature"
          ],
          "authors": [
            "Marsh, Laura F."
          ],
          "publicationDate": "2012",
          "jacket": {
            "type": "SYNDETICS",
            "small": "https://secure.syndetics.com/index.aspx?isbn=9781426312786/SC.GIF&client=sepup&type=xw12&oclc=",
            "medium": "https://secure.syndetics.com/index.aspx?isbn=9781426312786/MC.GIF&client=sepup&type=xw12&oclc=",
            "large": "https://secure.syndetics.com/index.aspx?isbn=9781426312786/LC.JPG&client=sepup&type=xw12&oclc=",
            "local_url": null
          },
          "isbns": [
            "9781426312786",
            "1426312784"
          ],
          "primaryLanguage": "eng",
          "series": [
            {
              "name": "National Geographic kids",
              "volume": null,
              "sortName": "national geographic kids",
              "volumeNumber": null
            },
            {
              "name": "National Geographic readers",
              "volume": null,
              "sortName": "national geographic readers",
              "volumeNumber": null
            },
            {
              "name": "National Geographic readers. Level 2",
              "volume": null,
              "sortName": "national geographic readers. level 2",
              "volumeNumber": null
            }
          ],
          "edition": null,
          "multiscriptTitle": null,
          "multiscriptAuthor": null,
          "id": "S30C3114974",
          "rating": {
            "averageRating": 95,
            "totalCount": 11
          }
        },
        "policy": {
          "materialType": "DIGITAL",
          "provider": "OverDriveAPI",
          "holdable": true,
          "itemLevelHoldableItems": false,
          "materialPolicy": {
            "supportsBatchVolumeHolds": false,
            "itemLevelHoldsPredetermined": false,
            "requiresDuration": false,
            "requiresEmailDuringHold": true,
            "requiresFormatDuringHold": false,
            "requiresFormatDuringCheckout": false,
            "supportsDownload": true,
            "supportsFastAvailability": false,
            "supportsReadNow": true,
            "showFormatDuringHold": true,
            "showFormatDuringCheckout": true,
            "supportsSuspendHold": true,
            "supportsSuspendHoldStartDateEdit": false,
            "supportsSuspendHoldEndDateEdit": true,
            "supportAutoCheckout": true,
            "showQuickstartLink": true,
            "supportsBorrowingHistory": false,
            "borrowingHistorySupportsReturnedDates": false,
            "maintainsBorrowingHistoryOnDisable": false,
            "supportsCancelledExpiredHolds": false,
            "helpUrl": "http://www.overdrive.com/resources/drc/Default.aspx?type=ebook",
            "quickStartUrl": "https://help.overdrive.com/#",
            "healthy": true,
            "externalAPI": true
          }
        },
        "availability": {
          "metadataId": "S30C3114974",
          "bibType": "DIGITAL",
          "availabilityLocationType": "OTHER",
          "status": "AVAILABLE",
          "circulationType": "REQUEST",
          "libraryUseOnly": false,
          "heldCopies": 0,
          "availableCopies": 1,
          "totalCopies": 1,
          "localisedStatus": "AVAILABLE",
          "singleBranch": false,
          "eresourceDescription": null,
          "eresourceUrl": null
        }
      },
      "S30C2276813": {
        "id": "S30C2276813",
        "sourceLibId": 30,
        "briefInfo": {
          "metadataId": "S30C2276813",
          "title": "Testing",
          "subtitle": "",
          "format": "BK",
          "superFormats": [
            "BOOKS",
            "MODERN_FORMATS"
          ],
          "consumptionFormat": "READ",
          "callNumber": "759.13 C3856T 2004",
          "description": null,
          "genreForm": [],
          "subjectHeadings": [
            "Painting, Modern 20th century"
          ],
          "authors": [
            "Chartier, Jaq"
          ],
          "publicationDate": "2004",
          "jacket": {
            "type": "SYNDETICS",
            "small": "https://secure.syndetics.com/index.aspx?isbn=9780974420219/SC.GIF&client=sepup&type=xw12&oclc=",
            "medium": "https://secure.syndetics.com/index.aspx?isbn=9780974420219/MC.GIF&client=sepup&type=xw12&oclc=",
            "large": "https://secure.syndetics.com/index.aspx?isbn=9780974420219/LC.JPG&client=sepup&type=xw12&oclc=",
            "local_url": null
          },
          "isbns": [
            "9780974420219",
            "0974420212"
          ],
          "primaryLanguage": "eng",
          "series": [],
          "edition": null,
          "multiscriptTitle": null,
          "multiscriptAuthor": null,
          "id": "S30C2276813",
          "rating": {
            "averageRating": 90,
            "totalCount": 1
          }
        },
        "policy": {
          "materialType": "PHYSICAL",
          "provider": "ILS",
          "holdable": true,
          "itemLevelHoldableItems": false,
          "materialPolicy": {
            "supportsBatchVolumeHolds": true,
            "itemLevelHoldsPredetermined": true,
            "requiresDuration": false,
            "requiresEmailDuringHold": false,
            "requiresFormatDuringHold": false,
            "requiresFormatDuringCheckout": false,
            "supportsDownload": false,
            "supportsFastAvailability": false,
            "supportsReadNow": false,
            "showFormatDuringHold": false,
            "showFormatDuringCheckout": false,
            "supportsSuspendHold": false,
            "supportsSuspendHoldStartDateEdit": false,
            "supportsSuspendHoldEndDateEdit": true,
            "supportAutoCheckout": false,
            "showQuickstartLink": false,
            "supportsBorrowingHistory": false,
            "borrowingHistorySupportsReturnedDates": true,
            "maintainsBorrowingHistoryOnDisable": false,
            "supportsCancelledExpiredHolds": true,
            "helpUrl": null,
            "quickStartUrl": null,
            "healthy": true,
            "externalAPI": false
          }
        },
        "availability": {
          "metadataId": "S30C2276813",
          "bibType": "PHYSICAL",
          "availabilityLocationType": "OTHER",
          "status": "AVAILABLE",
          "circulationType": "REQUEST",
          "libraryUseOnly": true,
          "heldCopies": 0,
          "availableCopies": 1,
          "totalCopies": 2,
          "localisedStatus": "LIBRARY_USE_ONLY_AT_THIS_TIME",
          "singleBranch": false,
          "eresourceDescription": null,
          "eresourceUrl": null
        }
      },
      "S30C2865729": {
        "id": "S30C2865729",
        "sourceLibId": 30,
        "briefInfo": {
          "metadataId": "S30C2865729",
          "title": "Until Tuesday",
          "subtitle": "A Wounded Warrior and the Golden Retriever Who Saved Him",
          "format": "EBOOK",
          "superFormats": [
            "BOOKS",
            "ELECTRONIC_FORMATS"
          ],
          "consumptionFormat": "READ",
          "callNumber": "EBOOK OVERDRIVE",
          "description": "\\"Tuesday has a personality that shines. I am not kidding when I say it is common for people to pull out their cell phones and take pictures of and with him. Tuesday is that kind of dog. And then, in passing, they notice me, the big man with the tight haircut. There is nothing about me--even the straight, stiff way I carry myself--that signals disabled. Until people notice the cane in my left hand, that is, and the way I lean on it every few steps. Then they realize my stiff walk and straight posture aren't just pride, and that Tuesday isn't just an ordinary dog. He walks directly beside me, for instance, so that my right leg always bisects his body. He nuzzles me when my breathing changes, and he moves immediately between me and the object--a cat, an overeager child, a suspiciously closed door--any time I feel apprehensive. Because beautiful, happy-go-lucky, favorite-of-the-neighborhood Tuesday isn't my pet; he's my service dog.\\" Captain Luis Montalvan returned home from his second tour of duty in Iraq, having survived stab wounds, a traumatic brain injury, and three broken vertebrae. But the pressures of civilian life and his injuries proved too much to bear. Physical disabilities, agoraphobia, and crippling PTSD drove him to the edge of suicide. That's when he met Tuesday - his best friend forever. Tuesday came with his own history of challenges: from the Puppies Behind Bars program, to a home for troubled boys, to the streets of Manhattan, Tuesday blessed many lives on his way to Luis. Until Tuesday unforgettably twines the story of man and dog\\"--",
          "genreForm": [
            "Biography",
            "Electronic books",
            "Electronic Books"
          ],
          "subjectHeadings": [
            "Montalván, Luis Carlos",
            "Service dogs United States",
            "Human-animal relationships",
            "Men with disabilities United States Biography",
            "People with disabilities Biography",
            "Dogs",
            "Veterans Rehabilitation United States Biography"
          ],
          "authors": [
            "Montalván, Luis Carlos"
          ],
          "publicationDate": "2011",
          "jacket": {
            "type": "SYNDETICS",
            "small": "https://secure.syndetics.com/index.aspx?isbn=9781401303761/SC.GIF&client=sepup&type=xw12&oclc=",
            "medium": "https://secure.syndetics.com/index.aspx?isbn=9781401303761/MC.GIF&client=sepup&type=xw12&oclc=",
            "large": "https://secure.syndetics.com/index.aspx?isbn=9781401303761/LC.JPG&client=sepup&type=xw12&oclc=",
            "local_url": null
          },
          "isbns": [
            "9781401303761",
            "1401303765"
          ],
          "primaryLanguage": "eng",
          "series": [],
          "edition": "First edition",
          "multiscriptTitle": null,
          "multiscriptAuthor": null,
          "id": "S30C2865729",
          "rating": {
            "averageRating": 84,
            "totalCount": 73
          }
        },
        "policy": {
          "materialType": "DIGITAL",
          "provider": "OverDriveAPI",
          "holdable": true,
          "itemLevelHoldableItems": false,
          "materialPolicy": {
            "supportsBatchVolumeHolds": false,
            "itemLevelHoldsPredetermined": false,
            "requiresDuration": false,
            "requiresEmailDuringHold": true,
            "requiresFormatDuringHold": false,
            "requiresFormatDuringCheckout": false,
            "supportsDownload": true,
            "supportsFastAvailability": false,
            "supportsReadNow": true,
            "showFormatDuringHold": true,
            "showFormatDuringCheckout": true,
            "supportsSuspendHold": true,
            "supportsSuspendHoldStartDateEdit": false,
            "supportsSuspendHoldEndDateEdit": true,
            "supportAutoCheckout": true,
            "showQuickstartLink": true,
            "supportsBorrowingHistory": false,
            "borrowingHistorySupportsReturnedDates": false,
            "maintainsBorrowingHistoryOnDisable": false,
            "supportsCancelledExpiredHolds": false,
            "helpUrl": "http://www.overdrive.com/resources/drc/Default.aspx?type=ebook",
            "quickStartUrl": "https://help.overdrive.com/#",
            "healthy": true,
            "externalAPI": true
          }
        },
        "availability": {
          "metadataId": "S30C2865729",
          "bibType": "DIGITAL",
          "availabilityLocationType": "OTHER",
          "status": "AVAILABLE",
          "circulationType": "REQUEST",
          "libraryUseOnly": false,
          "heldCopies": 0,
          "availableCopies": 2,
          "totalCopies": 2,
          "localisedStatus": "AVAILABLE",
          "singleBranch": false,
          "eresourceDescription": null,
          "eresourceUrl": null
        }
      },
      "S30C3269716": {
        "id": "S30C3269716",
        "sourceLibId": 30,
        "briefInfo": {
          "metadataId": "S30C3269716",
          "title": "Beyond Tuesday Morning",
          "subtitle": "",
          "format": "BK",
          "superFormats": [
            "BOOKS",
            "MODERN_FORMATS"
          ],
          "consumptionFormat": "READ",
          "callNumber": "FIC KINGSBU 2004",
          "description": "Three years have passed since the terrorist attacks on New York City. Jamie Bryan, widow of a firefighter who lost his life on that terrible day, has found meaning in her season of loss by volunteering at St. Paul's, the memorial chapel across the street from where the Twin Towers once stood. Here she meets a daily stream of people touched by the tragedy, including two men with whom she feels a connection. One is a firefighter also changed by the attacks, the other a police officer from Los Angeles. But as Jamie gets to know the police officer, she is stunned to find out that he is the brother of Eric Michaels, the man with the uncanny resemblance to Jamie's husband, the man who lived with her for three months after September 11. Eric is the man she has vowed never to see again. Certain she could not share even a friendship with his brother, Jamie shuts out the police officer and delves deeper into her work at St. Paul's. Now it will take the persistence of a tenacious man, the questions from her curious young daughter, and the words from her dead husband's journal to move Jamie beyond one Tuesday morning.",
          "genreForm": [
            "Christian fiction",
            "Christian Fiction",
            "Fiction"
          ],
          "subjectHeadings": [
            "September 11 Terrorist Attacks, 2001 Fiction",
            "Terrorism victims' families Fiction",
            "Fire fighters Fiction",
            "Brothers Fiction",
            "Widows Fiction",
            "Police Fiction",
            "New York (N.Y.) Fiction"
          ],
          "authors": [
            "Kingsbury, Karen"
          ],
          "publicationDate": "2004",
          "jacket": {
            "type": "SYNDETICS",
            "small": "https://secure.syndetics.com/index.aspx?isbn=9780310257714/SC.GIF&client=sepup&type=xw12&oclc=",
            "medium": "https://secure.syndetics.com/index.aspx?isbn=9780310257714/MC.GIF&client=sepup&type=xw12&oclc=",
            "large": "https://secure.syndetics.com/index.aspx?isbn=9780310257714/LC.JPG&client=sepup&type=xw12&oclc=",
            "local_url": null
          },
          "isbns": [
            "9780310257714",
            "0310257719"
          ],
          "primaryLanguage": "eng",
          "series": [
            {
              "name": "Kingsbury, Karen. September 11 series",
              "volume": null,
              "sortName": "kingsbury, karen. september 11 series",
              "volumeNumber": null
            },
            {
              "name": "September 11 series",
              "volume": null,
              "sortName": "september 11 series",
              "volumeNumber": null
            }
          ],
          "edition": null,
          "multiscriptTitle": null,
          "multiscriptAuthor": null,
          "id": "S30C3269716",
          "rating": {
            "averageRating": 89,
            "totalCount": 58
          }
        },
        "policy": {
          "materialType": "PHYSICAL",
          "provider": "ILS",
          "holdable": true,
          "itemLevelHoldableItems": false,
          "materialPolicy": {
            "supportsBatchVolumeHolds": true,
            "itemLevelHoldsPredetermined": true,
            "requiresDuration": false,
            "requiresEmailDuringHold": false,
            "requiresFormatDuringHold": false,
            "requiresFormatDuringCheckout": false,
            "supportsDownload": false,
            "supportsFastAvailability": false,
            "supportsReadNow": false,
            "showFormatDuringHold": false,
            "showFormatDuringCheckout": false,
            "supportsSuspendHold": false,
            "supportsSuspendHoldStartDateEdit": false,
            "supportsSuspendHoldEndDateEdit": true,
            "supportAutoCheckout": false,
            "showQuickstartLink": false,
            "supportsBorrowingHistory": false,
            "borrowingHistorySupportsReturnedDates": true,
            "maintainsBorrowingHistoryOnDisable": false,
            "supportsCancelledExpiredHolds": true,
            "helpUrl": null,
            "quickStartUrl": null,
            "healthy": true,
            "externalAPI": false
          }
        },
        "availability": {
          "metadataId": "S30C3269716",
          "bibType": "PHYSICAL",
          "availabilityLocationType": "OTHER",
          "status": "AVAILABLE",
          "circulationType": "REQUEST",
          "libraryUseOnly": false,
          "heldCopies": 0,
          "availableCopies": 4,
          "totalCopies": 5,
          "localisedStatus": "AVAILABLE_IN_BRANCH",
          "singleBranch": false,
          "eresourceDescription": null,
          "eresourceUrl": null
        }
      },
      "S30C2740645": {
        "id": "S30C2740645",
        "sourceLibId": 30,
        "briefInfo": {
          "metadataId": "S30C2740645",
          "title": "Until Tuesday",
          "subtitle": "A Wounded Warrior and the Golden Retriever Who Saved Him",
          "format": "LPRINT",
          "superFormats": [
            "BOOKS",
            "ACCESSIBLE_FORMATS"
          ],
          "consumptionFormat": "READ",
          "callNumber": "362.4092 M761U 2011",
          "description": null,
          "genreForm": [
            "Biography"
          ],
          "subjectHeadings": [
            "Montalván, Luis Carlos",
            "Service dogs United States",
            "Human-animal relationships",
            "Men with disabilities United States Biography",
            "Large type books"
          ],
          "authors": [
            "Montalván, Luis Carlos"
          ],
          "publicationDate": "2011",
          "jacket": {
            "type": "SYNDETICS",
            "small": "https://secure.syndetics.com/index.aspx?isbn=9781410441935/SC.GIF&client=sepup&type=xw12&oclc=",
            "medium": "https://secure.syndetics.com/index.aspx?isbn=9781410441935/MC.GIF&client=sepup&type=xw12&oclc=",
            "large": "https://secure.syndetics.com/index.aspx?isbn=9781410441935/LC.JPG&client=sepup&type=xw12&oclc=",
            "local_url": null
          },
          "isbns": [
            "9781410441935",
            "1410441938"
          ],
          "primaryLanguage": "eng",
          "series": [
            {
              "name": "Thorndike Press large print nonfiction series",
              "volume": null,
              "sortName": "thorndike press large print nonfiction series",
              "volumeNumber": null
            },
            {
              "name": "Thorndike Press large print nonfiction",
              "volume": null,
              "sortName": "thorndike press large print nonfiction",
              "volumeNumber": null
            }
          ],
          "edition": "Large print edition",
          "multiscriptTitle": null,
          "multiscriptAuthor": null,
          "id": "S30C2740645",
          "rating": {
            "averageRating": 84,
            "totalCount": 73
          }
        },
        "policy": {
          "materialType": "PHYSICAL",
          "provider": "ILS",
          "holdable": true,
          "itemLevelHoldableItems": false,
          "materialPolicy": {
            "supportsBatchVolumeHolds": true,
            "itemLevelHoldsPredetermined": true,
            "requiresDuration": false,
            "requiresEmailDuringHold": false,
            "requiresFormatDuringHold": false,
            "requiresFormatDuringCheckout": false,
            "supportsDownload": false,
            "supportsFastAvailability": false,
            "supportsReadNow": false,
            "showFormatDuringHold": false,
            "showFormatDuringCheckout": false,
            "supportsSuspendHold": false,
            "supportsSuspendHoldStartDateEdit": false,
            "supportsSuspendHoldEndDateEdit": true,
            "supportAutoCheckout": false,
            "showQuickstartLink": false,
            "supportsBorrowingHistory": false,
            "borrowingHistorySupportsReturnedDates": true,
            "maintainsBorrowingHistoryOnDisable": false,
            "supportsCancelledExpiredHolds": true,
            "helpUrl": null,
            "quickStartUrl": null,
            "healthy": true,
            "externalAPI": false
          }
        },
        "availability": {
          "metadataId": "S30C2740645",
          "bibType": "PHYSICAL",
          "availabilityLocationType": "OTHER",
          "status": "AVAILABLE",
          "circulationType": "REQUEST",
          "libraryUseOnly": false,
          "heldCopies": 0,
          "availableCopies": 1,
          "totalCopies": 1,
          "localisedStatus": "AVAILABLE_IN_BRANCH",
          "singleBranch": false,
          "eresourceDescription": null,
          "eresourceUrl": null
        }
      },
      "S30C189543": {
        "id": "S30C189543",
        "sourceLibId": 30,
        "briefInfo": {
          "metadataId": "S30C189543",
          "title": "\\"Fat Tuesday\\" Report, 1979",
          "subtitle": "",
          "format": "BK",
          "superFormats": [
            "BOOKS",
            "MODERN_FORMATS"
          ],
          "consumptionFormat": "READ",
          "callNumber": "R363.32 Se18F",
          "description": null,
          "genreForm": [],
          "subjectHeadings": [
            "Seattle (Wash.). Police Department",
            "Police Washington (State) Seattle",
            "Police misconduct Washington (State) Seattle",
            "Shrove Tuesday Washington (State) Seattle",
            "Carnival Washington (State) Seattle",
            "Crowds Washington (State) Seattle",
            "Riots Washington (State) Seattle",
            "Pioneer Square (Seattle, Wash.)"
          ],
          "authors": [
            "Seattle (Wash.)"
          ],
          "publicationDate": "1979",
          "jacket": {
            "type": "SYNDETICS",
            "small": null,
            "medium": null,
            "large": null,
            "local_url": null
          },
          "isbns": [],
          "primaryLanguage": "eng",
          "series": [],
          "edition": null,
          "multiscriptTitle": null,
          "multiscriptAuthor": null,
          "id": "S30C189543",
          "rating": {
            "averageRating": 100,
            "totalCount": 1
          }
        },
        "policy": {
          "materialType": "PHYSICAL",
          "provider": "ILS",
          "holdable": false,
          "itemLevelHoldableItems": false,
          "materialPolicy": {
            "supportsBatchVolumeHolds": true,
            "itemLevelHoldsPredetermined": true,
            "requiresDuration": false,
            "requiresEmailDuringHold": false,
            "requiresFormatDuringHold": false,
            "requiresFormatDuringCheckout": false,
            "supportsDownload": false,
            "supportsFastAvailability": false,
            "supportsReadNow": false,
            "showFormatDuringHold": false,
            "showFormatDuringCheckout": false,
            "supportsSuspendHold": false,
            "supportsSuspendHoldStartDateEdit": false,
            "supportsSuspendHoldEndDateEdit": true,
            "supportAutoCheckout": false,
            "showQuickstartLink": false,
            "supportsBorrowingHistory": false,
            "borrowingHistorySupportsReturnedDates": true,
            "maintainsBorrowingHistoryOnDisable": false,
            "supportsCancelledExpiredHolds": true,
            "helpUrl": null,
            "quickStartUrl": null,
            "healthy": true,
            "externalAPI": false
          }
        },
        "availability": {
          "metadataId": "S30C189543",
          "bibType": "PHYSICAL",
          "availabilityLocationType": "OTHER",
          "status": "AVAILABLE",
          "circulationType": "NON_CIRCULATING",
          "libraryUseOnly": true,
          "heldCopies": 0,
          "availableCopies": 1,
          "totalCopies": 1,
          "localisedStatus": "LIBRARY_USE_ONLY",
          "singleBranch": false,
          "eresourceDescription": null,
          "eresourceUrl": null
        }
      },
      "S30C2082387": {
        "id": "S30C2082387",
        "sourceLibId": 30,
        "briefInfo": {
          "metadataId": "S30C2082387",
          "title": "An Independent Review of the Mardi Gras/\\"Fat Tuesday\\" Riots in Seattle, Washington, February 23-27, 2001",
          "subtitle": "",
          "format": "BK",
          "superFormats": [
            "BOOKS",
            "MODERN_FORMATS"
          ],
          "consumptionFormat": "READ",
          "callNumber": "364.14309 N213i",
          "description": null,
          "genreForm": [],
          "subjectHeadings": [
            "Seattle (Wash.). Police Department",
            "Riots Washington (State) Seattle",
            "Crowds Washington (State) Seattle",
            "Police Washington (State) Seattle",
            "Police misconduct Washington (State) Seattle",
            "Shrove Tuesday Washington (State) Seattle",
            "Carnival Washington (State) Seattle",
            "Pioneer Square (Seattle, Wash.)"
          ],
          "authors": [
            "National Tactical Officers Association (U.S.)"
          ],
          "publicationDate": "2001",
          "jacket": {
            "type": "SYNDETICS",
            "small": null,
            "medium": null,
            "large": null,
            "local_url": null
          },
          "isbns": [],
          "primaryLanguage": "eng",
          "series": [],
          "edition": null,
          "multiscriptTitle": null,
          "multiscriptAuthor": null,
          "id": "S30C2082387",
          "rating": {
            "averageRating": 100,
            "totalCount": 1
          }
        },
        "policy": {
          "materialType": "PHYSICAL",
          "provider": "ILS",
          "holdable": false,
          "itemLevelHoldableItems": false,
          "materialPolicy": {
            "supportsBatchVolumeHolds": true,
            "itemLevelHoldsPredetermined": true,
            "requiresDuration": false,
            "requiresEmailDuringHold": false,
            "requiresFormatDuringHold": false,
            "requiresFormatDuringCheckout": false,
            "supportsDownload": false,
            "supportsFastAvailability": false,
            "supportsReadNow": false,
            "showFormatDuringHold": false,
            "showFormatDuringCheckout": false,
            "supportsSuspendHold": false,
            "supportsSuspendHoldStartDateEdit": false,
            "supportsSuspendHoldEndDateEdit": true,
            "supportAutoCheckout": false,
            "showQuickstartLink": false,
            "supportsBorrowingHistory": false,
            "borrowingHistorySupportsReturnedDates": true,
            "maintainsBorrowingHistoryOnDisable": false,
            "supportsCancelledExpiredHolds": true,
            "helpUrl": null,
            "quickStartUrl": null,
            "healthy": true,
            "externalAPI": false
          }
        },
        "availability": {
          "metadataId": "S30C2082387",
          "bibType": "PHYSICAL",
          "availabilityLocationType": "OTHER",
          "status": "AVAILABLE",
          "circulationType": "NON_CIRCULATING",
          "libraryUseOnly": true,
          "heldCopies": 0,
          "availableCopies": 1,
          "totalCopies": 1,
          "localisedStatus": "LIBRARY_USE_ONLY",
          "singleBranch": false,
          "eresourceDescription": null,
          "eresourceUrl": null
        }
      },
      "S30C2720187": {
        "id": "S30C2720187",
        "sourceLibId": 30,
        "briefInfo": {
          "metadataId": "S30C2720187",
          "title": "Until Tuesday",
          "subtitle": "A Wounded Warrior and the Golden Retriever Who Saved Him",
          "format": "BK",
          "superFormats": [
            "BOOKS",
            "MODERN_FORMATS"
          ],
          "consumptionFormat": "READ",
          "callNumber": "362.4092 M761U 2011",
          "description": "\\"Tuesday has a personality that shines. I am not kidding when I say it is common for people to pull out their cell phones and take pictures of and with him. Tuesday is that kind of dog. And then, in passing, they notice me, the big man with the tight haircut. There is nothing about me--even the straight, stiff way I carry myself--that signals disabled. Until people notice the cane in my left hand, that is, and the way I lean on it every few steps. Then they realize my stiff walk and straight posture aren't just pride, and that Tuesday isn't just an ordinary dog. He walks directly beside me, for instance, so that my right leg always bisects his body. He nuzzles me when my breathing changes, and he moves immediately between me and the object--a cat, an overeager child, a suspiciously closed door--any time I feel apprehensive. Because beautiful, happy-go-lucky, favorite-of-the-neighborhood Tuesday isn't my pet; he's my service dog.\\" Captain Luis Montalvan returned home from his second tour of duty in Iraq, having survived stab wounds, a traumatic brain injury, and three broken vertebrae. But the pressures of civilian life and his injuries proved too much to bear. Physical disabilities, agoraphobia, and crippling PTSD drove him to the edge of suicide. That's when he met Tuesday - his best friend forever. Tuesday came with his own history of challenges: from the Puppies Behind Bars program, to a home for troubled boys, to the streets of Manhattan, Tuesday blessed many lives on his way to Luis. Until Tuesday unforgettably twines the story of man and dog\\"--Provided by publisher.",
          "genreForm": [
            "Biography"
          ],
          "subjectHeadings": [
            "Montalván, Luis Carlos",
            "Service dogs United States",
            "Human-animal relationships",
            "Men with disabilities United States Biography"
          ],
          "authors": [
            "Montalván, Luis Carlos"
          ],
          "publicationDate": "2011",
          "jacket": {
            "type": "SYNDETICS",
            "small": "https://secure.syndetics.com/index.aspx?isbn=9781401324292/SC.GIF&client=sepup&type=xw12&oclc=",
            "medium": "https://secure.syndetics.com/index.aspx?isbn=9781401324292/MC.GIF&client=sepup&type=xw12&oclc=",
            "large": "https://secure.syndetics.com/index.aspx?isbn=9781401324292/LC.JPG&client=sepup&type=xw12&oclc=",
            "local_url": null
          },
          "isbns": [
            "9781401324292",
            "1401324290"
          ],
          "primaryLanguage": "eng",
          "series": [],
          "edition": "First edition",
          "multiscriptTitle": null,
          "multiscriptAuthor": null,
          "id": "S30C2720187",
          "rating": {
            "averageRating": 84,
            "totalCount": 73
          }
        },
        "policy": {
          "materialType": "PHYSICAL",
          "provider": "ILS",
          "holdable": true,
          "itemLevelHoldableItems": false,
          "materialPolicy": {
            "supportsBatchVolumeHolds": true,
            "itemLevelHoldsPredetermined": true,
            "requiresDuration": false,
            "requiresEmailDuringHold": false,
            "requiresFormatDuringHold": false,
            "requiresFormatDuringCheckout": false,
            "supportsDownload": false,
            "supportsFastAvailability": false,
            "supportsReadNow": false,
            "showFormatDuringHold": false,
            "showFormatDuringCheckout": false,
            "supportsSuspendHold": false,
            "supportsSuspendHoldStartDateEdit": false,
            "supportsSuspendHoldEndDateEdit": true,
            "supportAutoCheckout": false,
            "showQuickstartLink": false,
            "supportsBorrowingHistory": false,
            "borrowingHistorySupportsReturnedDates": true,
            "maintainsBorrowingHistoryOnDisable": false,
            "supportsCancelledExpiredHolds": true,
            "helpUrl": null,
            "quickStartUrl": null,
            "healthy": true,
            "externalAPI": false
          }
        },
        "availability": {
          "metadataId": "S30C2720187",
          "bibType": "PHYSICAL",
          "availabilityLocationType": "OTHER",
          "status": "AVAILABLE",
          "circulationType": "REQUEST",
          "libraryUseOnly": false,
          "heldCopies": 0,
          "availableCopies": 4,
          "totalCopies": 4,
          "localisedStatus": "AVAILABLE_IN_BRANCH",
          "singleBranch": false,
          "eresourceDescription": null,
          "eresourceUrl": null
        }
      },
      "S30C3277054": {
        "id": "S30C3277054",
        "sourceLibId": 30,
        "briefInfo": {
          "metadataId": "S30C3277054",
          "title": "See You Next Tuesday",
          "subtitle": "",
          "format": "GRAPHIC_NOVEL",
          "superFormats": [
            "BOOKS",
            "MODERN_FORMATS"
          ],
          "consumptionFormat": "OTHER",
          "callNumber": "741.5973 M28M 2015",
          "description": "\\"This collection of diary comics features the ennui and wee of twenty-something Jane Mai whose emotions and art traverse the high and low. Moments of visual poetry and heartbreak are interspersed by bad body hair and bathroom disasters--much like life.\\"--Amazon.com.",
          "genreForm": [
            "Autobiographical comics",
            "Graphic Novels",
            "Biography<delimit>Comic Books, Strips, Etc",
            "Graphic novels",
            "Comic Books, Strips, Etc",
            "Autobiographical Comics"
          ],
          "subjectHeadings": [
            "Mai, Jane Comic books, strips, etc",
            "Cartoonists United States Biography Comic books, strips, etc",
            "Young women Conduct of life Comic books, strips, etc",
            "Diaries Comic books, strips, etc"
          ],
          "authors": [
            "Mai, Jane"
          ],
          "publicationDate": "2015",
          "jacket": {
            "type": "SYNDETICS",
            "small": "https://secure.syndetics.com/index.aspx?isbn=9781927668252/SC.GIF&client=sepup&type=xw12&oclc=",
            "medium": "https://secure.syndetics.com/index.aspx?isbn=9781927668252/MC.GIF&client=sepup&type=xw12&oclc=",
            "large": "https://secure.syndetics.com/index.aspx?isbn=9781927668252/LC.JPG&client=sepup&type=xw12&oclc=",
            "local_url": null
          },
          "isbns": [
            "9781927668252",
            "1927668255"
          ],
          "primaryLanguage": "eng",
          "series": [],
          "edition": "First edition",
          "multiscriptTitle": null,
          "multiscriptAuthor": null,
          "id": "S30C3277054",
          "rating": {
            "averageRating": 58,
            "totalCount": 7
          }
        },
        "policy": {
          "materialType": "PHYSICAL",
          "provider": "ILS",
          "holdable": true,
          "itemLevelHoldableItems": false,
          "materialPolicy": {
            "supportsBatchVolumeHolds": true,
            "itemLevelHoldsPredetermined": true,
            "requiresDuration": false,
            "requiresEmailDuringHold": false,
            "requiresFormatDuringHold": false,
            "requiresFormatDuringCheckout": false,
            "supportsDownload": false,
            "supportsFastAvailability": false,
            "supportsReadNow": false,
            "showFormatDuringHold": false,
            "showFormatDuringCheckout": false,
            "supportsSuspendHold": false,
            "supportsSuspendHoldStartDateEdit": false,
            "supportsSuspendHoldEndDateEdit": true,
            "supportAutoCheckout": false,
            "showQuickstartLink": false,
            "supportsBorrowingHistory": false,
            "borrowingHistorySupportsReturnedDates": true,
            "maintainsBorrowingHistoryOnDisable": false,
            "supportsCancelledExpiredHolds": true,
            "helpUrl": null,
            "quickStartUrl": null,
            "healthy": true,
            "externalAPI": false
          }
        },
        "availability": {
          "metadataId": "S30C3277054",
          "bibType": "PHYSICAL",
          "availabilityLocationType": "OTHER",
          "status": "AVAILABLE",
          "circulationType": "REQUEST",
          "libraryUseOnly": false,
          "heldCopies": 0,
          "availableCopies": 3,
          "totalCopies": 3,
          "localisedStatus": "AVAILABLE_IN_BRANCH",
          "singleBranch": false,
          "eresourceDescription": null,
          "eresourceUrl": null
        }
      },
      "S27C581032": {
        "id": "S27C581032",
        "sourceLibId": 27,
        "briefInfo": {
          "metadataId": "S27C581032",
          "title": "Ships in the Field",
          "subtitle": "",
          "format": "BK",
          "superFormats": [
            "BOOKS",
            "MODERN_FORMATS"
          ],
          "consumptionFormat": "READ",
          "callNumber": "J PIC",
          "description": "Australia is an immigrant nation with many stories. Award-winning author Susanne Gervay and award-winning illustrator Anna Pignataro are part of that immigrant and refugee story. Susanne's parents were post-war Hungarian refugees who migrated to Australia. Anna's parents were postwar Italian refugees who migrated to Australia. In a unique collaboration, Susanne and Anna have created a moving and significant picture book, Ships in the Field. It crosses boundaries in a universal recognition that children are part of the journey of war, migration, loss and healing. Through warmth, humour, pathos and story within story, it breaks the silence, engaging children, families and community.",
          "genreForm": [
            "Pictorial Works<delimit>Juvenile Fiction",
            "Pictorial Works"
          ],
          "subjectHeadings": [
            "Ships Pictorial works Juvenile fiction.",
            "Immigrant children Pictorial works Juvenile fiction.",
            "War victims Pictorial works Juvenile fiction.",
            "Children's stories Pictorial works."
          ],
          "authors": [
            "Gervay, Susanne"
          ],
          "publicationDate": "2011",
          "jacket": {
            "type": "SYNDETICS",
            "small": "https://secure.syndetics.com/index.aspx?isbn=9781921665233/SC.GIF&client=sepup&type=xw12&oclc=",
            "medium": "https://secure.syndetics.com/index.aspx?isbn=9781921665233/MC.GIF&client=sepup&type=xw12&oclc=",
            "large": "https://secure.syndetics.com/index.aspx?isbn=9781921665233/LC.JPG&client=sepup&type=xw12&oclc=",
            "local_url": null
          },
          "isbns": [
            "9781921665233",
            "9781921665509"
          ],
          "primaryLanguage": "eng",
          "series": [],
          "edition": null,
          "multiscriptTitle": null,
          "multiscriptAuthor": null,
          "id": "S27C581032",
          "rating": {
            "averageRating": 75,
            "totalCount": 2
          }
        },
        "policy": {
          "materialType": "PHYSICAL",
          "provider": "ILS",
          "holdable": false,
          "itemLevelHoldableItems": false,
          "materialPolicy": {
            "supportsBatchVolumeHolds": true,
            "itemLevelHoldsPredetermined": true,
            "requiresDuration": false,
            "requiresEmailDuringHold": false,
            "requiresFormatDuringHold": false,
            "requiresFormatDuringCheckout": false,
            "supportsDownload": false,
            "supportsFastAvailability": false,
            "supportsReadNow": false,
            "showFormatDuringHold": false,
            "showFormatDuringCheckout": false,
            "supportsSuspendHold": false,
            "supportsSuspendHoldStartDateEdit": false,
            "supportsSuspendHoldEndDateEdit": true,
            "supportAutoCheckout": false,
            "showQuickstartLink": false,
            "supportsBorrowingHistory": false,
            "borrowingHistorySupportsReturnedDates": true,
            "maintainsBorrowingHistoryOnDisable": false,
            "supportsCancelledExpiredHolds": true,
            "helpUrl": null,
            "quickStartUrl": null,
            "healthy": true,
            "externalAPI": false
          }
        },
        "availability": {
          "metadataId": "S27C581032",
          "bibType": "PHYSICAL",
          "availabilityLocationType": "NA",
          "status": "UNAVAILABLE",
          "circulationType": "REQUEST",
          "libraryUseOnly": false,
          "heldCopies": 0,
          "availableCopies": 0,
          "totalCopies": 2,
          "localisedStatus": "UNAVAILABLE",
          "singleBranch": false,
          "eresourceDescription": null,
          "eresourceUrl": null
        }
      },
      "S30C3111184": {
        "id": "S30C3111184",
        "sourceLibId": 30,
        "briefInfo": {
          "metadataId": "S30C3111184",
          "title": "Say Hello!",
          "subtitle": "",
          "format": "BK",
          "superFormats": [
            "BOOKS",
            "MODERN_FORMATS"
          ],
          "consumptionFormat": "READ",
          "callNumber": "E DAVICK",
          "description": "Illustrations and rhyming text reveal that \\"hello\\" can be said many ways, from a handshake to a dance of happiness, and that the world would be a lot more fun if more hellos were shared.",
          "genreForm": [
            "Juvenile Literature",
            "Fiction"
          ],
          "subjectHeadings": [
            "Salutations Juvenile literature",
            "Stories in rhyme",
            "Salutations Fiction"
          ],
          "authors": [
            "Davick, Linda"
          ],
          "publicationDate": "2015",
          "jacket": {
            "type": "SYNDETICS",
            "small": "https://secure.syndetics.com/index.aspx?isbn=9781481428675/SC.GIF&client=sepup&type=xw12&oclc=",
            "medium": "https://secure.syndetics.com/index.aspx?isbn=9781481428675/MC.GIF&client=sepup&type=xw12&oclc=",
            "large": "https://secure.syndetics.com/index.aspx?isbn=9781481428675/LC.JPG&client=sepup&type=xw12&oclc=",
            "local_url": null
          },
          "isbns": [
            "9781481428675",
            "1481428675"
          ],
          "primaryLanguage": "eng",
          "series": [],
          "edition": "First edition",
          "multiscriptTitle": null,
          "multiscriptAuthor": null,
          "id": "S30C3111184",
          "rating": {
            "averageRating": 74,
            "totalCount": 14
          }
        },
        "policy": {
          "materialType": "PHYSICAL",
          "provider": "ILS",
          "holdable": true,
          "itemLevelHoldableItems": false,
          "materialPolicy": {
            "supportsBatchVolumeHolds": true,
            "itemLevelHoldsPredetermined": true,
            "requiresDuration": false,
            "requiresEmailDuringHold": false,
            "requiresFormatDuringHold": false,
            "requiresFormatDuringCheckout": false,
            "supportsDownload": false,
            "supportsFastAvailability": false,
            "supportsReadNow": false,
            "showFormatDuringHold": false,
            "showFormatDuringCheckout": false,
            "supportsSuspendHold": false,
            "supportsSuspendHoldStartDateEdit": false,
            "supportsSuspendHoldEndDateEdit": true,
            "supportAutoCheckout": false,
            "showQuickstartLink": false,
            "supportsBorrowingHistory": false,
            "borrowingHistorySupportsReturnedDates": true,
            "maintainsBorrowingHistoryOnDisable": false,
            "supportsCancelledExpiredHolds": true,
            "helpUrl": null,
            "quickStartUrl": null,
            "healthy": true,
            "externalAPI": false
          }
        },
        "availability": {
          "metadataId": "S30C3111184",
          "bibType": "PHYSICAL",
          "availabilityLocationType": "OTHER",
          "status": "AVAILABLE",
          "circulationType": "REQUEST",
          "libraryUseOnly": false,
          "heldCopies": 0,
          "availableCopies": 5,
          "totalCopies": 8,
          "localisedStatus": "AVAILABLE_IN_BRANCH",
          "singleBranch": false,
          "eresourceDescription": null,
          "eresourceUrl": null
        }
      },
      "S30C3076526": {
        "id": "S30C3076526",
        "sourceLibId": 30,
        "briefInfo": {
          "metadataId": "S30C3076526",
          "title": "The Book With No Pictures",
          "subtitle": "",
          "format": "AB",
          "superFormats": [
            "AUDIOBOOKS_SPOKEN_WORD",
            "ELECTRONIC_FORMATS",
            "TALKING_BOOKS"
          ],
          "consumptionFormat": "LISTEN",
          "callNumber": "EAUDIO J OVERDRIVE",
          "description": "This innovative and wildly funny audiobook by award-winning humorist/actor B.J. Novak will turn any listener into a comedian. You might think a book with no pictures seems boring and serious. Except . . . here's how books work. Everything written on the page has to be said by the person reading it aloud. Even if the words say ... BLORK. Or BLUURF... Even if the words are a preposterous song about eating ants for breakfast, or just a list of astonishingly goofy sounds like blaggity blaggity and glibbity globbity.",
          "genreForm": [
            "Children's audiobooks",
            "Children's Audiobooks",
            "Downloadable audio books",
            "Downloadable Audio Books",
            "Juvenile Fiction"
          ],
          "subjectHeadings": [
            "Books and reading Juvenile fiction",
            "Humorous stories"
          ],
          "authors": [
            "Novak, B. J."
          ],
          "publicationDate": "2014",
          "jacket": {
            "type": "SYNDETICS",
            "small": "https://secure.syndetics.com/index.aspx?isbn=9780553397093/SC.GIF&client=sepup&type=xw12&oclc=",
            "medium": "https://secure.syndetics.com/index.aspx?isbn=9780553397093/MC.GIF&client=sepup&type=xw12&oclc=",
            "large": "https://secure.syndetics.com/index.aspx?isbn=9780553397093/LC.JPG&client=sepup&type=xw12&oclc=",
            "local_url": null
          },
          "isbns": [
            "9780553397093",
            "0553397095"
          ],
          "primaryLanguage": "eng",
          "series": [],
          "edition": "Unabridged",
          "multiscriptTitle": null,
          "multiscriptAuthor": null,
          "id": "S30C3076526",
          "rating": {
            "averageRating": 87,
            "totalCount": 498
          }
        },
        "policy": {
          "materialType": "DIGITAL",
          "provider": "OverDriveAPI",
          "holdable": true,
          "itemLevelHoldableItems": false,
          "materialPolicy": {
            "supportsBatchVolumeHolds": false,
            "itemLevelHoldsPredetermined": false,
            "requiresDuration": false,
            "requiresEmailDuringHold": true,
            "requiresFormatDuringHold": false,
            "requiresFormatDuringCheckout": false,
            "supportsDownload": true,
            "supportsFastAvailability": false,
            "supportsReadNow": true,
            "showFormatDuringHold": true,
            "showFormatDuringCheckout": true,
            "supportsSuspendHold": true,
            "supportsSuspendHoldStartDateEdit": false,
            "supportsSuspendHoldEndDateEdit": true,
            "supportAutoCheckout": true,
            "showQuickstartLink": true,
            "supportsBorrowingHistory": false,
            "borrowingHistorySupportsReturnedDates": false,
            "maintainsBorrowingHistoryOnDisable": false,
            "supportsCancelledExpiredHolds": false,
            "helpUrl": "http://www.overdrive.com/resources/drc/Default.aspx?type=ebook",
            "quickStartUrl": "https://help.overdrive.com/#",
            "healthy": true,
            "externalAPI": true
          }
        },
        "availability": {
          "metadataId": "S30C3076526",
          "bibType": "DIGITAL",
          "availabilityLocationType": "OTHER",
          "status": "UNAVAILABLE",
          "circulationType": "REQUEST",
          "libraryUseOnly": false,
          "heldCopies": 1,
          "availableCopies": 0,
          "totalCopies": 2,
          "localisedStatus": "UNAVAILABLE",
          "singleBranch": false,
          "eresourceDescription": null,
          "eresourceUrl": null
        }
      },
      "S30C3332471": {
        "id": "S30C3332471",
        "sourceLibId": 30,
        "briefInfo": {
          "metadataId": "S30C3332471",
          "title": "The Enchanted Clock",
          "subtitle": "A Novel",
          "format": "BK",
          "superFormats": [
            "BOOKS",
            "MODERN_FORMATS"
          ],
          "consumptionFormat": "READ",
          "callNumber": "FIC KRISTEV 2017",
          "description": "\\"In the Palace of Versailles there is a fabulous golden clock, made for Louis XV by the king's engineer, Claude-Siméon Passemant. The astronomical clock shows the phases of the moon and the movements of the planets, and it will tell time--hours, minutes, seconds, and even sixtieths of seconds--until the year 9999. ...Nivi Delisle, a psychoanalyst and magazine editor, nearly drowns while swimming off the Île de Ré; the astrophysicist Theo Passemant fishes her out of the water. They become lovers. While Theo wonders if he is descended from the clockmaker Passemant, Niv's son Stan, who suffers from occasional comas, develops a passion for the remarkable clock at Versailles. Soon Nivi is fixated on its maker. But then the clock is stolen, and when a young writer for Nivi's magazine mysteriously dies, the clock is found near his body.\\"--",
          "genreForm": [
            "Historical Fiction",
            "Detective and mystery fiction",
            "Detective and Mystery Fiction",
            "Fiction",
            "Historical fiction"
          ],
          "subjectHeadings": [
            "Passemant, Claude Siméon, 1702-1769 Fiction",
            "Astronomical clocks France Fiction",
            "Time Fiction"
          ],
          "authors": [
            "Kristeva, Julia"
          ],
          "publicationDate": "2017",
          "jacket": {
            "type": "SYNDETICS",
            "small": "https://secure.syndetics.com/index.aspx?isbn=9780231180467/SC.GIF&client=sepup&type=xw12&oclc=",
            "medium": "https://secure.syndetics.com/index.aspx?isbn=9780231180467/MC.GIF&client=sepup&type=xw12&oclc=",
            "large": "https://secure.syndetics.com/index.aspx?isbn=9780231180467/LC.JPG&client=sepup&type=xw12&oclc=",
            "local_url": null
          },
          "isbns": [
            "9780231180467",
            "0231180462"
          ],
          "primaryLanguage": "eng",
          "series": [],
          "edition": null,
          "multiscriptTitle": null,
          "multiscriptAuthor": null,
          "id": "S30C3332471",
          "rating": {}
        },
        "policy": {
          "materialType": "PHYSICAL",
          "provider": "ILS",
          "holdable": true,
          "itemLevelHoldableItems": false,
          "materialPolicy": {
            "supportsBatchVolumeHolds": true,
            "itemLevelHoldsPredetermined": true,
            "requiresDuration": false,
            "requiresEmailDuringHold": false,
            "requiresFormatDuringHold": false,
            "requiresFormatDuringCheckout": false,
            "supportsDownload": false,
            "supportsFastAvailability": false,
            "supportsReadNow": false,
            "showFormatDuringHold": false,
            "showFormatDuringCheckout": false,
            "supportsSuspendHold": false,
            "supportsSuspendHoldStartDateEdit": false,
            "supportsSuspendHoldEndDateEdit": true,
            "supportAutoCheckout": false,
            "showQuickstartLink": false,
            "supportsBorrowingHistory": false,
            "borrowingHistorySupportsReturnedDates": true,
            "maintainsBorrowingHistoryOnDisable": false,
            "supportsCancelledExpiredHolds": true,
            "helpUrl": null,
            "quickStartUrl": null,
            "healthy": true,
            "externalAPI": false
          }
        },
        "availability": {
          "metadataId": "S30C3332471",
          "bibType": "PHYSICAL",
          "availabilityLocationType": "OTHER",
          "status": "AVAILABLE",
          "circulationType": "REQUEST",
          "libraryUseOnly": false,
          "heldCopies": 0,
          "availableCopies": 3,
          "totalCopies": 4,
          "localisedStatus": "AVAILABLE_IN_BRANCH",
          "singleBranch": false,
          "eresourceDescription": null,
          "eresourceUrl": null
        }
      },
      "S30C2682085": {
        "id": "S30C2682085",
        "sourceLibId": 30,
        "briefInfo": {
          "metadataId": "S30C2682085",
          "title": "Yes You Can!",
          "subtitle": "Your Guide to Becoming An Activist",
          "format": "BK",
          "superFormats": [
            "BOOKS",
            "MODERN_FORMATS"
          ],
          "consumptionFormat": "READ",
          "callNumber": "YA 361.2083 D7894Y 2010",
          "description": "The authors present the nine steps to successful social change.",
          "genreForm": [
            "Juvenile Literature"
          ],
          "subjectHeadings": [
            "Social change Juvenile literature",
            "Social action Juvenile literature"
          ],
          "authors": [
            "Drake, Jane"
          ],
          "publicationDate": "2010",
          "jacket": {
            "type": "SYNDETICS",
            "small": "https://secure.syndetics.com/index.aspx?isbn=9780887769429/SC.GIF&client=sepup&type=xw12&oclc=",
            "medium": "https://secure.syndetics.com/index.aspx?isbn=9780887769429/MC.GIF&client=sepup&type=xw12&oclc=",
            "large": "https://secure.syndetics.com/index.aspx?isbn=9780887769429/LC.JPG&client=sepup&type=xw12&oclc=",
            "local_url": null
          },
          "isbns": [
            "9780887769429",
            "088776942X"
          ],
          "primaryLanguage": "eng",
          "series": [],
          "edition": null,
          "multiscriptTitle": null,
          "multiscriptAuthor": null,
          "id": "S30C2682085",
          "rating": {
            "averageRating": 80,
            "totalCount": 1
          }
        },
        "policy": {
          "materialType": "PHYSICAL",
          "provider": "ILS",
          "holdable": true,
          "itemLevelHoldableItems": false,
          "materialPolicy": {
            "supportsBatchVolumeHolds": true,
            "itemLevelHoldsPredetermined": true,
            "requiresDuration": false,
            "requiresEmailDuringHold": false,
            "requiresFormatDuringHold": false,
            "requiresFormatDuringCheckout": false,
            "supportsDownload": false,
            "supportsFastAvailability": false,
            "supportsReadNow": false,
            "showFormatDuringHold": false,
            "showFormatDuringCheckout": false,
            "supportsSuspendHold": false,
            "supportsSuspendHoldStartDateEdit": false,
            "supportsSuspendHoldEndDateEdit": true,
            "supportAutoCheckout": false,
            "showQuickstartLink": false,
            "supportsBorrowingHistory": false,
            "borrowingHistorySupportsReturnedDates": true,
            "maintainsBorrowingHistoryOnDisable": false,
            "supportsCancelledExpiredHolds": true,
            "helpUrl": null,
            "quickStartUrl": null,
            "healthy": true,
            "externalAPI": false
          }
        },
        "availability": {
          "metadataId": "S30C2682085",
          "bibType": "PHYSICAL",
          "availabilityLocationType": "OTHER",
          "status": "AVAILABLE",
          "circulationType": "REQUEST",
          "libraryUseOnly": false,
          "heldCopies": 0,
          "availableCopies": 3,
          "totalCopies": 3,
          "localisedStatus": "AVAILABLE_IN_BRANCH",
          "singleBranch": false,
          "eresourceDescription": null,
          "eresourceUrl": null
        }
      },
      "S30C3111817": {
        "id": "S30C3111817",
        "sourceLibId": 30,
        "briefInfo": {
          "metadataId": "S30C3111817",
          "title": "Blue",
          "subtitle": "The LAPD and the Battle to Redeem American Policing",
          "format": "BK",
          "superFormats": [
            "BOOKS",
            "MODERN_FORMATS"
          ],
          "consumptionFormat": "READ",
          "callNumber": "363.20979 D71B 2015",
          "description": "American policing is in crisis. Here, award-winning investigative journalist Joe Domanick reveals the troubled history of American policing over the past quarter century. He begins in the early 1990s with the beating of Rodney King and the L.A. riots, when the Los Angeles Police Department was caught between a corrupt and racist past and the demands of a rapidly changing urban population. Across the country, American cities faced similar challenges to law and order. In New York, William J. Bratton was spearheading the reorganization of the New York City Transit Police and later the 35,000-strong New York Police Department. His efforts resulted in a dramatic decrease in crime, yet introduced highly controversial policing strategies. In 2002, when Bratton was named the LAPD's new chief, he implemented the lessons learned in New York to change a department that previously had been impervious to reform. Blue ends in 2015 with the LAPD on its unfinished road to reform, as events in Los Angeles, New York, Baltimore, and Ferguson, Missouri, raise alarms about the very strategies Bratton pioneered, and about aggressive racial profiling and the militarization of police departments throughout the United States. Domanick tells his story through the lives of the people who lived it. Along with Bratton, he introduces William Parker, the legendary LAPD police chief; Tom Bradley, the first black mayor of Los Angeles; and Charlie Beck, the hard-nosed ex-gang cop who replaced Bratton as LAPD chief. The result is both intimate and expansive: a gripping narrative that asks big questions about what constitutes good and bad policing and how best to prevent crime, control police abuse, and ease tensions between the police and the powerless. Blue is not only a page-turning read but an essential addition to our scholarship.--Adapted from book jacket.",
          "genreForm": [],
          "subjectHeadings": [
            "Los Angeles (Calif.). Police Department",
            "Police California Los Angeles History",
            "Police misconduct California Los Angeles History",
            "Police administration California Los Angeles History",
            "Criminal justice, Administration of California Los Angeles History"
          ],
          "authors": [
            "Domanick, Joe"
          ],
          "publicationDate": "2015",
          "jacket": {
            "type": "SYNDETICS",
            "small": "https://secure.syndetics.com/index.aspx?isbn=9781451641073/SC.GIF&client=sepup&type=xw12&oclc=",
            "medium": "https://secure.syndetics.com/index.aspx?isbn=9781451641073/MC.GIF&client=sepup&type=xw12&oclc=",
            "large": "https://secure.syndetics.com/index.aspx?isbn=9781451641073/LC.JPG&client=sepup&type=xw12&oclc=",
            "local_url": null
          },
          "isbns": [
            "9781451641073",
            "1451641079"
          ],
          "primaryLanguage": "eng",
          "series": [],
          "edition": "First Simon & Schuster hardcover edition",
          "multiscriptTitle": null,
          "multiscriptAuthor": null,
          "id": "S30C3111817",
          "rating": {
            "averageRating": 80,
            "totalCount": 1
          }
        },
        "policy": {
          "materialType": "PHYSICAL",
          "provider": "ILS",
          "holdable": true,
          "itemLevelHoldableItems": false,
          "materialPolicy": {
            "supportsBatchVolumeHolds": true,
            "itemLevelHoldsPredetermined": true,
            "requiresDuration": false,
            "requiresEmailDuringHold": false,
            "requiresFormatDuringHold": false,
            "requiresFormatDuringCheckout": false,
            "supportsDownload": false,
            "supportsFastAvailability": false,
            "supportsReadNow": false,
            "showFormatDuringHold": false,
            "showFormatDuringCheckout": false,
            "supportsSuspendHold": false,
            "supportsSuspendHoldStartDateEdit": false,
            "supportsSuspendHoldEndDateEdit": true,
            "supportAutoCheckout": false,
            "showQuickstartLink": false,
            "supportsBorrowingHistory": false,
            "borrowingHistorySupportsReturnedDates": true,
            "maintainsBorrowingHistoryOnDisable": false,
            "supportsCancelledExpiredHolds": true,
            "helpUrl": null,
            "quickStartUrl": null,
            "healthy": true,
            "externalAPI": false
          }
        },
        "availability": {
          "metadataId": "S30C3111817",
          "bibType": "PHYSICAL",
          "availabilityLocationType": "OTHER",
          "status": "AVAILABLE",
          "circulationType": "REQUEST",
          "libraryUseOnly": false,
          "heldCopies": 0,
          "availableCopies": 2,
          "totalCopies": 2,
          "localisedStatus": "AVAILABLE_IN_BRANCH",
          "singleBranch": false,
          "eresourceDescription": null,
          "eresourceUrl": null
        }
      },
      "S30C2794669": {
        "id": "S30C2794669",
        "sourceLibId": 30,
        "briefInfo": {
          "metadataId": "S30C2794669",
          "title": "Blue",
          "subtitle": "",
          "format": "GRAPHIC_NOVEL",
          "superFormats": [
            "BOOKS",
            "MODERN_FORMATS"
          ],
          "consumptionFormat": "OTHER",
          "callNumber": "741.5994 G7676B 2012",
          "description": "Blue is the debut graphic novel of Australian cartoonist Pat Grant. Part autobiography, part science fiction, Blue is the story of three spotty teenagers who skip school to go surfing, only to end up investigating rumors of a dead body in their beach town.",
          "genreForm": [
            "Graphic Novels",
            "Science Fiction Comic Books, Strips, Etc",
            "Graphic novels",
            "Comic Books, Strips, Etc",
            "Science fiction comic books, strips, etc"
          ],
          "subjectHeadings": [
            "Teenagers Australia Comic books, strips, etc",
            "Aliens Comic books, strips, etc",
            "Surfing Comic books, strips, etc"
          ],
          "authors": [
            "Grant, Pat"
          ],
          "publicationDate": "2012",
          "jacket": {
            "type": "SYNDETICS",
            "small": "https://secure.syndetics.com/index.aspx?isbn=9781603091534/SC.GIF&client=sepup&type=xw12&oclc=",
            "medium": "https://secure.syndetics.com/index.aspx?isbn=9781603091534/MC.GIF&client=sepup&type=xw12&oclc=",
            "large": "https://secure.syndetics.com/index.aspx?isbn=9781603091534/LC.JPG&client=sepup&type=xw12&oclc=",
            "local_url": null
          },
          "isbns": [
            "9781603091534",
            "160309153X"
          ],
          "primaryLanguage": "eng",
          "series": [],
          "edition": null,
          "multiscriptTitle": null,
          "multiscriptAuthor": null,
          "id": "S30C2794669",
          "rating": {
            "averageRating": 87,
            "totalCount": 8
          }
        },
        "policy": {
          "materialType": "PHYSICAL",
          "provider": "ILS",
          "holdable": true,
          "itemLevelHoldableItems": false,
          "materialPolicy": {
            "supportsBatchVolumeHolds": true,
            "itemLevelHoldsPredetermined": true,
            "requiresDuration": false,
            "requiresEmailDuringHold": false,
            "requiresFormatDuringHold": false,
            "requiresFormatDuringCheckout": false,
            "supportsDownload": false,
            "supportsFastAvailability": false,
            "supportsReadNow": false,
            "showFormatDuringHold": false,
            "showFormatDuringCheckout": false,
            "supportsSuspendHold": false,
            "supportsSuspendHoldStartDateEdit": false,
            "supportsSuspendHoldEndDateEdit": true,
            "supportAutoCheckout": false,
            "showQuickstartLink": false,
            "supportsBorrowingHistory": false,
            "borrowingHistorySupportsReturnedDates": true,
            "maintainsBorrowingHistoryOnDisable": false,
            "supportsCancelledExpiredHolds": true,
            "helpUrl": null,
            "quickStartUrl": null,
            "healthy": true,
            "externalAPI": false
          }
        },
        "availability": {
          "metadataId": "S30C2794669",
          "bibType": "PHYSICAL",
          "availabilityLocationType": "OTHER",
          "status": "AVAILABLE",
          "circulationType": "REQUEST",
          "libraryUseOnly": false,
          "heldCopies": 0,
          "availableCopies": 1,
          "totalCopies": 1,
          "localisedStatus": "AVAILABLE_IN_BRANCH",
          "singleBranch": false,
          "eresourceDescription": null,
          "eresourceUrl": null
        }
      },
      "S30C3227296": {
        "id": "S30C3227296",
        "sourceLibId": 30,
        "briefInfo": {
          "metadataId": "S30C3227296",
          "title": "Fantastic Beasts and Where to Find Them",
          "subtitle": "The Original Screenplay",
          "format": "EBOOK",
          "superFormats": [
            "BOOKS",
            "ELECTRONIC_FORMATS"
          ],
          "consumptionFormat": "READ",
          "callNumber": "EBOOK OVERDRIVE",
          "description": "When Magizoologist Newt Scamander arrives in New York, he intends his stay to be just a brief stopover. However, when his magical case is misplaced and some of Newt's fantastic beasts escape, it spells trouble for everyone... Inspired by the original Hogwart's textbook by Newt Scamander, Fantastic Beasts and Where to Find Them: The Original Screenplay marks the screenwriting debut of J.K. Rowling, author of the beloved and internationally bestselling Harry Potter books. A feat of imagination and featuring a cast of remarkable characters and magical creatures, this is epic adventure-packed storytelling at its very best. Whether an existing fan or new to the wizarding world, this is a perfect addition for any film lover or reader's bookshelf. The film Fantastic Beasts and Where to Find Them will have its theatrical release on 18th November 2016.",
          "genreForm": [
            "Electronic books",
            "Electronic Books"
          ],
          "subjectHeadings": [],
          "authors": [
            "Rowling, J. K."
          ],
          "publicationDate": "2016",
          "jacket": {
            "type": "SYNDETICS",
            "small": "https://secure.syndetics.com/index.aspx?isbn=9781781109601/SC.GIF&client=sepup&type=xw12&oclc=",
            "medium": "https://secure.syndetics.com/index.aspx?isbn=9781781109601/MC.GIF&client=sepup&type=xw12&oclc=",
            "large": "https://secure.syndetics.com/index.aspx?isbn=9781781109601/LC.JPG&client=sepup&type=xw12&oclc=",
            "local_url": "http://images.contentreserve.com/ImageType-100/3450-1/%7BD8EAE59F-5F0B-449C-91AD-15168BB40334%7DImg100.jpg"
          },
          "isbns": [
            "9781781109601"
          ],
          "primaryLanguage": "eng",
          "series": [
            {
              "name": "Harry Potter",
              "volume": null,
              "sortName": "harry potter",
              "volumeNumber": null
            }
          ],
          "edition": null,
          "multiscriptTitle": null,
          "multiscriptAuthor": null,
          "id": "S30C3227296",
          "rating": {
            "averageRating": 83,
            "totalCount": 677
          }
        },
        "policy": {
          "materialType": "DIGITAL",
          "provider": "OverDriveAPI",
          "holdable": true,
          "itemLevelHoldableItems": false,
          "materialPolicy": {
            "supportsBatchVolumeHolds": false,
            "itemLevelHoldsPredetermined": false,
            "requiresDuration": false,
            "requiresEmailDuringHold": true,
            "requiresFormatDuringHold": false,
            "requiresFormatDuringCheckout": false,
            "supportsDownload": true,
            "supportsFastAvailability": false,
            "supportsReadNow": true,
            "showFormatDuringHold": true,
            "showFormatDuringCheckout": true,
            "supportsSuspendHold": true,
            "supportsSuspendHoldStartDateEdit": false,
            "supportsSuspendHoldEndDateEdit": true,
            "supportAutoCheckout": true,
            "showQuickstartLink": true,
            "supportsBorrowingHistory": false,
            "borrowingHistorySupportsReturnedDates": false,
            "maintainsBorrowingHistoryOnDisable": false,
            "supportsCancelledExpiredHolds": false,
            "helpUrl": "http://www.overdrive.com/resources/drc/Default.aspx?type=ebook",
            "quickStartUrl": "https://help.overdrive.com/#",
            "healthy": true,
            "externalAPI": true
          }
        },
        "availability": {
          "metadataId": "S30C3227296",
          "bibType": "DIGITAL",
          "availabilityLocationType": "OTHER",
          "status": "UNAVAILABLE",
          "circulationType": "REQUEST",
          "libraryUseOnly": false,
          "heldCopies": 2,
          "availableCopies": 0,
          "totalCopies": 30,
          "localisedStatus": "UNAVAILABLE",
          "singleBranch": false,
          "eresourceDescription": null,
          "eresourceUrl": null
        }
      },
      "S30C222093": {
        "id": "S30C222093",
        "sourceLibId": 30,
        "briefInfo": {
          "metadataId": "S30C222093",
          "title": "The Invisible Web",
          "subtitle": "Strange Tales of the French Sûreté",
          "format": "BK",
          "superFormats": [
            "BOOKS",
            "MODERN_FORMATS"
          ],
          "consumptionFormat": "READ",
          "callNumber": "364 As39i",
          "description": null,
          "genreForm": [],
          "subjectHeadings": [
            "Crime France Lyon",
            "Police France Lyon",
            "Criminals France Lyon"
          ],
          "authors": [
            "Ashton-Wolfe, H."
          ],
          "publicationDate": "1929",
          "jacket": {
            "type": "SYNDETICS",
            "small": null,
            "medium": null,
            "large": null,
            "local_url": null
          },
          "isbns": [],
          "primaryLanguage": "eng",
          "series": [],
          "edition": null,
          "multiscriptTitle": null,
          "multiscriptAuthor": null,
          "id": "S30C222093",
          "rating": {}
        },
        "policy": {
          "materialType": "PHYSICAL",
          "provider": "ILS",
          "holdable": true,
          "itemLevelHoldableItems": false,
          "materialPolicy": {
            "supportsBatchVolumeHolds": true,
            "itemLevelHoldsPredetermined": true,
            "requiresDuration": false,
            "requiresEmailDuringHold": false,
            "requiresFormatDuringHold": false,
            "requiresFormatDuringCheckout": false,
            "supportsDownload": false,
            "supportsFastAvailability": false,
            "supportsReadNow": false,
            "showFormatDuringHold": false,
            "showFormatDuringCheckout": false,
            "supportsSuspendHold": false,
            "supportsSuspendHoldStartDateEdit": false,
            "supportsSuspendHoldEndDateEdit": true,
            "supportAutoCheckout": false,
            "showQuickstartLink": false,
            "supportsBorrowingHistory": false,
            "borrowingHistorySupportsReturnedDates": true,
            "maintainsBorrowingHistoryOnDisable": false,
            "supportsCancelledExpiredHolds": true,
            "helpUrl": null,
            "quickStartUrl": null,
            "healthy": true,
            "externalAPI": false
          }
        },
        "availability": {
          "metadataId": "S30C222093",
          "bibType": "PHYSICAL",
          "availabilityLocationType": "OTHER",
          "status": "AVAILABLE",
          "circulationType": "REQUEST",
          "libraryUseOnly": false,
          "heldCopies": 0,
          "availableCopies": 1,
          "totalCopies": 1,
          "localisedStatus": "AVAILABLE_IN_BRANCH",
          "singleBranch": false,
          "eresourceDescription": null,
          "eresourceUrl": null
        }
      },
      "S30C3217063": {
        "id": "S30C3217063",
        "sourceLibId": 30,
        "briefInfo": {
          "metadataId": "S30C3217063",
          "title": "Psycho",
          "subtitle": "",
          "format": "BOOK_CD",
          "superFormats": [
            "AUDIOBOOKS_SPOKEN_WORD",
            "TALKING_BOOKS",
            "MODERN_FORMATS"
          ],
          "consumptionFormat": "LISTEN",
          "callNumber": "CD FIC BLOCH",
          "description": "It was a dark and stormy night when Mary Crane glimpsed the unlit neon sign announcing the vacancy at the Bates Motel. Exhausted, lost, and at the end of her rope, she was eager for a hot shower and a bed for the night. Her room was musty, but clean, and the manager seemed nice, if a little odd. This classic horror novel, which inspired the famous film by Alfred Hitchcock, has been thrilling people for fifty years. It introduced one of the most unexpectedly-twisted villains of all time in Norman Bates, the reserved motel manager with a mother complex, and has been called the first psychoanalytic thriller.",
          "genreForm": [
            "Horror fiction",
            "Psychological fiction",
            "Audiobooks",
            "Horror Fiction",
            "Fiction",
            "Psychological Fiction"
          ],
          "subjectHeadings": [
            "Murderers Fiction",
            "Bates, Norman (Fictitious character) Fiction",
            "Hotelkeepers Fiction"
          ],
          "authors": [
            "Bloch, Robert"
          ],
          "publicationDate": "2012",
          "jacket": {
            "type": "SYNDETICS",
            "small": "https://secure.syndetics.com/index.aspx?isbn=9781433257087/SC.GIF&client=sepup&type=xw12&oclc=",
            "medium": "https://secure.syndetics.com/index.aspx?isbn=9781433257087/MC.GIF&client=sepup&type=xw12&oclc=",
            "large": "https://secure.syndetics.com/index.aspx?isbn=9781433257087/LC.JPG&client=sepup&type=xw12&oclc=",
            "local_url": null
          },
          "isbns": [
            "9781433257087",
            "1433257084"
          ],
          "primaryLanguage": "eng",
          "series": [],
          "edition": "Unabridged",
          "multiscriptTitle": null,
          "multiscriptAuthor": null,
          "id": "S30C3217063",
          "rating": {
            "averageRating": 62,
            "totalCount": 5
          }
        },
        "policy": {
          "materialType": "PHYSICAL",
          "provider": "ILS",
          "holdable": true,
          "itemLevelHoldableItems": false,
          "materialPolicy": {
            "supportsBatchVolumeHolds": true,
            "itemLevelHoldsPredetermined": true,
            "requiresDuration": false,
            "requiresEmailDuringHold": false,
            "requiresFormatDuringHold": false,
            "requiresFormatDuringCheckout": false,
            "supportsDownload": false,
            "supportsFastAvailability": false,
            "supportsReadNow": false,
            "showFormatDuringHold": false,
            "showFormatDuringCheckout": false,
            "supportsSuspendHold": false,
            "supportsSuspendHoldStartDateEdit": false,
            "supportsSuspendHoldEndDateEdit": true,
            "supportAutoCheckout": false,
            "showQuickstartLink": false,
            "supportsBorrowingHistory": false,
            "borrowingHistorySupportsReturnedDates": true,
            "maintainsBorrowingHistoryOnDisable": false,
            "supportsCancelledExpiredHolds": true,
            "helpUrl": null,
            "quickStartUrl": null,
            "healthy": true,
            "externalAPI": false
          }
        },
        "availability": {
          "metadataId": "S30C3217063",
          "bibType": "PHYSICAL",
          "availabilityLocationType": "OTHER",
          "status": "AVAILABLE",
          "circulationType": "REQUEST",
          "libraryUseOnly": false,
          "heldCopies": 0,
          "availableCopies": 2,
          "totalCopies": 2,
          "localisedStatus": "AVAILABLE_IN_BRANCH",
          "singleBranch": false,
          "eresourceDescription": null,
          "eresourceUrl": null
        }
      },
      "S30C2957013": {
        "id": "S30C2957013",
        "sourceLibId": 30,
        "briefInfo": {
          "metadataId": "S30C2957013",
          "title": "Twitterature",
          "subtitle": "The World's Greatest Books in Twenty Tweets or Less",
          "format": "EBOOK",
          "superFormats": [
            "BOOKS",
            "ELECTRONIC_FORMATS"
          ],
          "consumptionFormat": "READ",
          "callNumber": "EBOOK OVERDRIVE",
          "description": null,
          "genreForm": [
            "Electronic books",
            "Humor",
            "Electronic Books"
          ],
          "subjectHeadings": [
            "Twitter",
            "Best books Humor"
          ],
          "authors": [
            "Aciman, Alexander"
          ],
          "publicationDate": "2009",
          "jacket": {
            "type": "SYNDETICS",
            "small": "https://secure.syndetics.com/index.aspx?isbn=9781101162194/SC.GIF&client=sepup&type=xw12&oclc=",
            "medium": "https://secure.syndetics.com/index.aspx?isbn=9781101162194/MC.GIF&client=sepup&type=xw12&oclc=",
            "large": "https://secure.syndetics.com/index.aspx?isbn=9781101162194/LC.JPG&client=sepup&type=xw12&oclc=",
            "local_url": null
          },
          "isbns": [
            "9781101162194",
            "1101162198",
            "9781101162828",
            "1101162821"
          ],
          "primaryLanguage": "eng",
          "series": [],
          "edition": null,
          "multiscriptTitle": null,
          "multiscriptAuthor": null,
          "id": "S30C2957013",
          "rating": {
            "averageRating": 49,
            "totalCount": 12
          }
        },
        "policy": {
          "materialType": "DIGITAL",
          "provider": "OverDriveAPI",
          "holdable": true,
          "itemLevelHoldableItems": false,
          "materialPolicy": {
            "supportsBatchVolumeHolds": false,
            "itemLevelHoldsPredetermined": false,
            "requiresDuration": false,
            "requiresEmailDuringHold": true,
            "requiresFormatDuringHold": false,
            "requiresFormatDuringCheckout": false,
            "supportsDownload": true,
            "supportsFastAvailability": false,
            "supportsReadNow": true,
            "showFormatDuringHold": true,
            "showFormatDuringCheckout": true,
            "supportsSuspendHold": true,
            "supportsSuspendHoldStartDateEdit": false,
            "supportsSuspendHoldEndDateEdit": true,
            "supportAutoCheckout": true,
            "showQuickstartLink": true,
            "supportsBorrowingHistory": false,
            "borrowingHistorySupportsReturnedDates": false,
            "maintainsBorrowingHistoryOnDisable": false,
            "supportsCancelledExpiredHolds": false,
            "helpUrl": "http://www.overdrive.com/resources/drc/Default.aspx?type=ebook",
            "quickStartUrl": "https://help.overdrive.com/#",
            "healthy": true,
            "externalAPI": true
          }
        },
        "availability": {
          "metadataId": "S30C2957013",
          "bibType": "DIGITAL",
          "availabilityLocationType": "OTHER",
          "status": "AVAILABLE",
          "circulationType": "REQUEST",
          "libraryUseOnly": false,
          "heldCopies": 0,
          "availableCopies": 1,
          "totalCopies": 1,
          "localisedStatus": "AVAILABLE",
          "singleBranch": false,
          "eresourceDescription": null,
          "eresourceUrl": null
        }
      },
      "S30C2199201": {
        "id": "S30C2199201",
        "sourceLibId": 30,
        "briefInfo": {
          "metadataId": "S30C2199201",
          "title": "Robin Williams DVD Design Workshop",
          "subtitle": "",
          "format": "BK",
          "superFormats": [
            "BOOKS",
            "MODERN_FORMATS"
          ],
          "consumptionFormat": "READ",
          "callNumber": "621.38832 W6754R 2003",
          "description": null,
          "genreForm": [],
          "subjectHeadings": [
            "DVDs"
          ],
          "authors": [
            "Williams, Robin"
          ],
          "publicationDate": "2004",
          "jacket": {
            "type": "SYNDETICS",
            "small": "https://secure.syndetics.com/index.aspx?isbn=9780321136282/SC.GIF&client=sepup&type=xw12&oclc=",
            "medium": "https://secure.syndetics.com/index.aspx?isbn=9780321136282/MC.GIF&client=sepup&type=xw12&oclc=",
            "large": "https://secure.syndetics.com/index.aspx?isbn=9780321136282/LC.JPG&client=sepup&type=xw12&oclc=",
            "local_url": null
          },
          "isbns": [
            "9780321136282",
            "0321136284"
          ],
          "primaryLanguage": "eng",
          "series": [],
          "edition": null,
          "multiscriptTitle": null,
          "multiscriptAuthor": null,
          "id": "S30C2199201",
          "rating": {
            "averageRating": 80,
            "totalCount": 1
          }
        },
        "policy": {
          "materialType": "PHYSICAL",
          "provider": "ILS",
          "holdable": true,
          "itemLevelHoldableItems": false,
          "materialPolicy": {
            "supportsBatchVolumeHolds": true,
            "itemLevelHoldsPredetermined": true,
            "requiresDuration": false,
            "requiresEmailDuringHold": false,
            "requiresFormatDuringHold": false,
            "requiresFormatDuringCheckout": false,
            "supportsDownload": false,
            "supportsFastAvailability": false,
            "supportsReadNow": false,
            "showFormatDuringHold": false,
            "showFormatDuringCheckout": false,
            "supportsSuspendHold": false,
            "supportsSuspendHoldStartDateEdit": false,
            "supportsSuspendHoldEndDateEdit": true,
            "supportAutoCheckout": false,
            "showQuickstartLink": false,
            "supportsBorrowingHistory": false,
            "borrowingHistorySupportsReturnedDates": true,
            "maintainsBorrowingHistoryOnDisable": false,
            "supportsCancelledExpiredHolds": true,
            "helpUrl": null,
            "quickStartUrl": null,
            "healthy": true,
            "externalAPI": false
          }
        },
        "availability": {
          "metadataId": "S30C2199201",
          "bibType": "PHYSICAL",
          "availabilityLocationType": "OTHER",
          "status": "AVAILABLE",
          "circulationType": "REQUEST",
          "libraryUseOnly": false,
          "heldCopies": 0,
          "availableCopies": 1,
          "totalCopies": 1,
          "localisedStatus": "AVAILABLE_IN_BRANCH",
          "singleBranch": false,
          "eresourceDescription": null,
          "eresourceUrl": null
        }
      }
    },
    "shelves": {
      "719393399": {
        "933187694": {
          "id": 933187694,
          "metadataId": "S30C2957013",
          "shelf": "in_progress",
          "privateItem": false,
          "dateAdded": "2017-05-01",
          "authors": [
            "Aciman, Alexander"
          ],
          "title": "Twitterature",
          "rating": 0
        },
        "933906277": {
          "id": 933906277,
          "metadataId": "S30C2199201",
          "shelf": "in_progress",
          "privateItem": false,
          "dateAdded": "2017-05-01",
          "authors": [
            "Williams, Robin"
          ],
          "title": "Robin Williams DVD Design Workshop",
          "rating": 8
        },
        "935072479": {
          "id": 935072479,
          "metadataId": "S30C3217063",
          "shelf": "in_progress",
          "privateItem": false,
          "dateAdded": "2017-05-02",
          "authors": [
            "Bloch, Robert"
          ],
          "title": "Psycho",
          "rating": 0
        },
        "1048567537": {
          "id": 1048567537,
          "metadataId": "S30C222093",
          "shelf": "in_progress",
          "privateItem": false,
          "dateAdded": "2017-09-08",
          "authors": [
            "Ashton-Wolfe, H."
          ],
          "title": "The Invisible Web",
          "rating": 0
        },
        "1077982549": {
          "id": 1077982549,
          "metadataId": "S30C3227296",
          "shelf": "in_progress",
          "privateItem": false,
          "dateAdded": "2017-11-01",
          "authors": [
            "Rowling, J. K."
          ],
          "title": "Fantastic Beasts and Where to Find Them",
          "rating": 0
        },
        "1114175944": {
          "id": 1114175944,
          "metadataId": "S30C2794669",
          "shelf": "in_progress",
          "privateItem": false,
          "dateAdded": "2018-01-03",
          "authors": [
            "Grant, Pat"
          ],
          "title": "Blue",
          "rating": 0
        },
        "1114177287": {
          "id": 1114177287,
          "metadataId": "S30C3111817",
          "shelf": "in_progress",
          "privateItem": false,
          "dateAdded": "2018-01-03",
          "authors": [
            "Domanick, Joe"
          ],
          "title": "Blue",
          "rating": 0
        },
        "1114177685": {
          "id": 1114177685,
          "metadataId": "S30C2682085",
          "shelf": "in_progress",
          "privateItem": false,
          "dateAdded": "2018-01-03",
          "authors": [
            "Drake, Jane"
          ],
          "title": "Yes You Can!",
          "rating": 0
        },
        "1124746344": {
          "id": 1124746344,
          "metadataId": "S30C3332471",
          "shelf": "in_progress",
          "privateItem": false,
          "dateAdded": "2018-01-21",
          "authors": [
            "Kristeva, Julia"
          ],
          "title": "The Enchanted Clock",
          "rating": 0
        },
        "1126049677": {
          "id": 1126049677,
          "metadataId": "S30C3076526",
          "shelf": "in_progress",
          "privateItem": false,
          "dateAdded": "2018-01-23",
          "authors": [
            "Novak, B. J."
          ],
          "title": "The Book With No Pictures",
          "rating": 0
        },
        "1162496497": {
          "id": 1162496497,
          "metadataId": "S30C3111184",
          "shelf": "in_progress",
          "privateItem": false,
          "dateAdded": "2018-03-26",
          "authors": [
            "Davick, Linda"
          ],
          "title": "Say Hello!",
          "rating": 0
        },
        "1162520457": {
          "id": 1162520457,
          "metadataId": "S27C581032",
          "shelf": "in_progress",
          "privateItem": false,
          "dateAdded": "2018-03-26",
          "authors": [
            "Gervay, Susanne"
          ],
          "title": "Ships in the Field",
          "rating": 0
        },
        "1167048397": {
          "id": 1167048397,
          "metadataId": "S30C3269716",
          "shelf": "in_progress",
          "privateItem": false,
          "dateAdded": "2018-04-03",
          "authors": [
            "Kingsbury, Karen"
          ],
          "title": "Beyond Tuesday Morning",
          "rating": 0
        },
        "1167048398": {
          "id": 1167048398,
          "metadataId": "S30C2865729",
          "shelf": "in_progress",
          "privateItem": false,
          "dateAdded": "2018-04-03",
          "authors": [
            "Montalván, Luis Carlos"
          ],
          "title": "Until Tuesday",
          "rating": 0
        },
        "1167048417": {
          "id": 1167048417,
          "metadataId": "S30C2720187",
          "shelf": "in_progress",
          "privateItem": false,
          "dateAdded": "2018-04-03",
          "authors": [
            "Montalván, Luis Carlos"
          ],
          "title": "Until Tuesday",
          "rating": 0
        },
        "1167048437": {
          "id": 1167048437,
          "metadataId": "S30C3277054",
          "shelf": "in_progress",
          "privateItem": false,
          "dateAdded": "2018-04-03",
          "authors": [
            "Mai, Jane"
          ],
          "title": "See You Next Tuesday",
          "rating": 0
        },
        "1167048438": {
          "id": 1167048438,
          "metadataId": "S30C189543",
          "shelf": "in_progress",
          "privateItem": false,
          "dateAdded": "2018-04-03",
          "authors": [
            "Seattle (Wash.)"
          ],
          "title": "\\"Fat Tuesday\\" Report, 1979",
          "rating": 0
        },
        "1167048517": {
          "id": 1167048517,
          "metadataId": "S30C2082387",
          "shelf": "in_progress",
          "privateItem": false,
          "dateAdded": "2018-04-03",
          "authors": [
            "National Tactical Officers Association (U.S.)"
          ],
          "title": "An Independent Review of the Mardi Gras/\\"Fat Tuesday\\" Riots in Seattle, Washington, February 23-27, 2001",
          "rating": 0
        },
        "1167048537": {
          "id": 1167048537,
          "metadataId": "S30C2740645",
          "shelf": "in_progress",
          "privateItem": false,
          "dateAdded": "2018-04-03",
          "authors": [
            "Montalván, Luis Carlos"
          ],
          "title": "Until Tuesday",
          "rating": 0
        },
        "1192570667": {
          "id": 1192570667,
          "metadataId": "S30C2276813",
          "shelf": "in_progress",
          "privateItem": false,
          "dateAdded": "2018-05-18",
          "authors": [
            "Chartier, Jaq"
          ],
          "title": "Testing",
          "rating": 0
        },
        "1192687269": {
          "id": 1192687269,
          "metadataId": "S30C3114974",
          "shelf": "in_progress",
          "privateItem": false,
          "dateAdded": "2018-05-29",
          "authors": [
            "Marsh, Laura F."
          ],
          "title": "Lizards",
          "rating": 0
        },
        "1192687271": {
          "id": 1192687271,
          "metadataId": "S30C3312725",
          "shelf": "in_progress",
          "privateItem": false,
          "dateAdded": "2018-05-29",
          "authors": [
            "Ludvicek, Zebo"
          ],
          "title": "Mouse",
          "rating": 0
        },
        "1225636297": {
          "id": 1225636297,
          "metadataId": "S30C3387619",
          "shelf": "in_progress",
          "privateItem": false,
          "dateAdded": "2018-07-11",
          "authors": [
            "Keller, A. G."
          ],
          "title": "Mia",
          "rating": 0
        },
        "1246957497": {
          "id": 1246957497,
          "metadataId": "S30C2221268",
          "shelf": "in_progress",
          "privateItem": false,
          "dateAdded": "2018-08-13",
          "authors": [
            "Gabriel, Peter"
          ],
          "title": "Hit",
          "rating": 0
        },
        "1260564627": {
          "id": 1260564627,
          "metadataId": "S30C3305179",
          "shelf": "in_progress",
          "privateItem": false,
          "dateAdded": "2018-09-04",
          "authors": [
            "King, Stephen"
          ],
          "title": "Sleeping Beauties",
          "rating": 0
        }
      }
    },
    "ugc": {
      "videos": {},
      "comments": {},
      "quotations": {},
      "ratings": {
        "933906280": {
          "id": 933906280,
          "metadataId": "S30C2199201",
          "rating": 80,
          "userId": 719393399
        }
      },
      "tags": {},
      "iOwnThis": {},
      "similarTitles": {}
    },
    "optIn": {
      "nerf_shelves": [
        "nerf_shelves"
      ]
    }
  }
}
  """;
  const bibJson =
  """
    {
        "id": "S30C3305179",
        "sourceLibId": 30
        }
  """;


  const jacketJson =
  """
    {
      "type" : " a type",
      "small" : "small image",
      "medium": "medium",
      "large": "large version"
    }
    
  """;
  test("Bib serialization", (){

  });

  test("Test full serialization",(){
    Jacket b = serializers.deserializeWith(Jacket.serializer, json.decode(jacketJson));
    print (b.type +  b.small);
  });

  test("Serialize jacket ", (){
    Jacket j = new Jacket((b) => b
        ..type = "a type"
        ..small = "small image"
        ..medium = "medium image"
        ..large = "large image"
      );
    print(json.encode(serializers.serializeWith(Jacket.serializer, j)));
  });
  
  test("full desrialze ", (){
    ShelvesPageRequest r = serializers.deserializeWith(ShelvesPageRequest.serializer, json.decode(fullRequest));
    assert("for_later" == r.summary.shelves[0].name);
  });
}